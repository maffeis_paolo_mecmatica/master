﻿Imports System.Data.Odbc
Imports Mecmatica.IntegrationUtility
Public Class frmMain
    Dim sTitolo As String = "Import EMMETI"

#Region "Variabili"
    Dim lTimerImport As Integer = 0

    Dim dsMetronomo As New DataSet
    Dim dsImport As New DataSet
    Dim cnMetronomo As SqlClient.SqlConnection
    Dim cnImport As OleDb.OleDbConnection


    Dim mecMisc As New MecmaticaMisc.MecmaticaMisc
    Dim mecDatabase As New MecmaticaDatabase.MecmaticaDatabase
    Dim MecmaticaCrypto As New MecmaticaCrypto.MecmaticaCrypto

    Dim trdBackground As Threading.Thread

    Dim lIDArticolo As Long
    Dim lIDODL As Long
    Dim lIDFase As Long
    Dim lIDCiclo As Long
    Dim lIDContatto As Long

    Dim dicNazioni As Dictionary(Of String, String)
    Dim da As SqlClient.SqlDataAdapter
    Dim dsddt As DataSet

    Dim moQueryPool As New ArrayList

#End Region

#Region "Delegate"
    Delegate Sub Log_Delegate(ByVal sMsg As String, ByVal bMain As Boolean)
    Delegate Sub TitoloDelegate(ByVal sMessaggio As String)
    Delegate Sub AddTable_Delegate(ByVal sTabella As ListViewItem)
    Delegate Sub DisableControl_Delegate(ByVal moControl As Control)
    Delegate Sub GenericSubDelegate()
    Delegate Sub Progresso_Delegate(ByVal i As Integer, ByVal Tot As Integer)
#End Region

#Region "Button"



    Private Sub btnEsci_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEsci.Click
        Me.Close()
    End Sub

    Private Sub btnCarica_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCarica.Click
        Dim trd As New Threading.Thread(AddressOf Carica)
        trd.Start()
    End Sub

    Private Sub btnEsegui_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEsegui.Click


        Carica(False)

        Esegui()
    End Sub

    Private Sub btnSalva_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalva.Click

        Dim dsConfig As New DataSet
        Dim tConfig As New DataTable
        tConfig.Columns.Add("MetronomoServer")
        tConfig.Columns.Add("MetronomoDB")
        tConfig.Columns.Add("MetronomoUser")
        tConfig.Columns.Add("MetronomoPassword")
        tConfig.Columns.Add("MetronomoComplex", GetType(Boolean))
        tConfig.Columns.Add("ImportServer")
        tConfig.Columns.Add("ImportDB")
        tConfig.Columns.Add("ImportUser")
        tConfig.Columns.Add("ImportPassword")
        tConfig.Columns.Add("TimerImportazione")
        tConfig.Columns.Add("SurcePath")


        Dim rRow As DataRow
        rRow = tConfig.NewRow
        rRow("MetronomoServer") = Me.txtMetronomoServer.Text
        rRow("MetronomoDB") = Me.txtMetronomoDB.Text
        rRow("MetronomoUser") = Me.txtMetronomoUser.Text
        rRow("MetronomoPassword") = MecmaticaCrypto.EncryptText(Me.txtMetronomoPassword.Text, sTitolo)
        'rRow("MetronomoComplex") = Me.chkMetronomoComplex.Checked
        rRow("ImportServer") = Me.txtImportServer.Text
        rRow("ImportDB") = Me.txtImportDB.Text
        rRow("ImportUser") = Me.txtImportUser.Text
        rRow("ImportPassword") = MecmaticaCrypto.EncryptText(Me.txtImportPassword.Text, sTitolo)
        rRow("TimerImportazione") = Me.txtTimerImportazione.Text
        rRow("SurcePath") = Me.SurceFilesPath.Text
        tConfig.Rows.Add(rRow)

        dsConfig.Tables.Add(tConfig)

        If mecMisc.MsgQuest("Salvare la nuova configurazione ?") Then
            If Not dsConfig Is Nothing Then
                dsConfig.WriteXml("Config.xml")
            End If
        End If
    End Sub
#End Region

#Region "lstTabelle"
    Private Sub lstTabelle_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstTabelle.SelectedIndexChanged
        Try
            If lstTabelle.SelectedItems.Count > 0 Then
                If lstTabelle.SelectedItems(0).SubItems(1).Text = "Import" Then
                    dbgFields.DataSource = dsImport.Tables(lstTabelle.SelectedItems(0).SubItems(0).Text)
                ElseIf lstTabelle.SelectedItems(0).SubItems(1).Text = "Metronomo" Then
                    dbgFields.DataSource = dsMetronomo.Tables(lstTabelle.SelectedItems(0).SubItems(0).Text)
                    'Else
                    '    dbgFields.DataSource = dsTemp.Tables(lstTabelle.SelectedItems(0).SubItems(0).Text)
                    '    lblCont.Text = "Contenuto tabella '" & lstTabelle.SelectedItems(0).SubItems(0).Text & "' / Righe " & CType(dbgFields.DataSource, DataTable).Rows.Count
                End If
            End If
        Catch ex As Exception
            mecMisc.MsgErr(ex.Message)
            LogMsg(ex.Message())
        End Try
    End Sub
#End Region

#Region "Funzioni Form"
    Sub Titolo(ByVal sMessaggio As String)
        If Me.InvokeRequired Then
            Dim d As New TitoloDelegate(AddressOf Titolo)
            Me.Invoke(d, New Object() {sMessaggio})
        Else
            If sMessaggio = "" Then
                Me.Text = sTitolo & " - " & Application.ProductVersion
            Else
                Me.Text = sTitolo & " - " & Application.ProductVersion & " - " & sMessaggio
            End If
            Application.DoEvents()
        End If
    End Sub
    Sub LogMsg(ByVal sMsg As String, Optional ByVal bMain As Boolean = False)
        If Me.InvokeRequired Then
            Dim d As New Log_Delegate(AddressOf LogMsg)
            Me.Invoke(d, New Object() {sMsg, bMain})
        Else
            If bMain Then
                txtLogMain.Text = Now & " >> " & sMsg & vbCrLf & txtLogMain.Text
            Else
                txtLog.Text = " - " & sMsg & vbCrLf & txtLog.Text
                If txtLog.Text.Length > 30000 Then '4000
                    txtLog.Text = ""
                End If
            End If
        End If
    End Sub
    Sub Progresso(ByVal i As Integer, ByVal Tot As Integer)
        If Me.InvokeRequired Then
            Dim d As New Progresso_Delegate(AddressOf Progresso)
            Me.Invoke(d, New Object() {i, Tot})
        Else
            If pgbProgresso.Maximum <> Tot Then
                pgbProgresso.Maximum = Tot
                pgbProgresso.Minimum = 0
            End If
            pgbProgresso.Value = i
            Titolo(i & " / " & Tot)
        End If
    End Sub
    Sub DisableControl(ByVal moControl As Control)
        If Me.InvokeRequired Then
            Dim d As New DisableControl_Delegate(AddressOf DisableControl)
            Me.Invoke(d, moControl)
        Else
            moControl.Enabled = False
        End If
    End Sub
#End Region

#Region "Form_Main"
    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Titolo("")

        Dim temp As Process() = Process.GetProcesses()
        Dim iIstanze As Int16
        Dim iProcessi As Integer
        For iProcessi = 0 To temp.Length - 1
            If InStr(UCase(temp(iProcessi).ProcessName), UCase(Application.ProductName)) Then iIstanze += 1
        Next
        If iIstanze > 1 Then
            mecMisc.MsgErr("Un'altra istanza di " & UCase(Application.ProductName) & " è in esecuzione su questo PC !")
            End
        End If


        If IO.File.Exists(Application.StartupPath & "\config.xml") Then
            Dim dsConfig As New DataSet
            dsConfig.ReadXml("config.xml")
            If dsConfig.Tables.Count > 0 Then
                Dim tConfig As DataTable
                tConfig = dsConfig.Tables(0)
                If tConfig.Rows.Count > 0 Then
                    Dim oRow As DataRow
                    oRow = tConfig.Rows(0)
                    txtMetronomoServer.Text = oRow("MetronomoServer")
                    txtMetronomoDB.Text = oRow("MetronomoDB")
                    txtMetronomoUser.Text = oRow("MetronomoUser")
                    txtMetronomoPassword.Text = MecmaticaCrypto.DecryptText(oRow("MetronomoPassword"), sTitolo)
                    'chkMetronomoComplex.Checked = oRow("MetronomoComplex")
                    txtImportServer.Text = oRow("ImportServer")
                    txtImportDB.Text = oRow("ImportDB")
                    txtImportUser.Text = oRow("ImportUser")
                    txtImportPassword.Text = MecmaticaCrypto.DecryptText(oRow("ImportPassword"), sTitolo)
                    txtTimerImportazione.Text = oRow("TimerImportazione")
                    SurceFilesPath.Text = oRow("SurcePath")
                End If
            End If
        End If

        CompilaFunzioni()

        lTimerImport = Val("" & txtTimerImportazione.Text)
        If lTimerImport > 0 Then
            trdBackground = New Threading.Thread(AddressOf Controllo)
            trdBackground.Name = "BG Thread"
            trdBackground.IsBackground = True
            trdBackground.Start()
        End If
    End Sub
#End Region

#Region "Funzioni Controllo \ Caricamento \ Utility connessione"
    Sub Controllo()
        LogMsg("Controllo importazione background avviato", True)
        LogMsg("Inizio tra 10 secondi...", True)
        Threading.Thread.Sleep(10 * 1000)
        LogMsg("...Avviato", True)

        While True

            Carica(False)
            Esegui()
            If Now.Hour > 22 Then
                LogMsg("Prossimo controllo tra 480 min.  - Pausa Notturna -", True)
                Titolo("Prossima esecuzione alle " & Now.AddMinutes(480))
                Threading.Thread.Sleep(480 * 60 * 1000)
            Else
                LogMsg("Prossimo controllo tra " & lTimerImport & " min.", True)
                Titolo("Prossima esecuzione alle " & Now.AddMinutes(lTimerImport))
                Threading.Thread.Sleep(lTimerImport * 60 * 1000)
            End If

        End While
        LogMsg("Terminato")
    End Sub

    Sub Carica(ByVal onlyMet As Boolean)
        'MsgBox("procedo")
        dsMetronomo.Tables.Clear()
        dsImport.Tables.Clear()

        SvuotaLista()

        'Dim pr As Process = Process.Start("c:\Condivisi\Contab\MAP\ESPORANAGRAFICHE.lnk")
        'MsgBox("proced1")

        'Threading.Thread.Sleep(2000)
        'SendKeys.Send("{ENTER}")

        'pr.WaitForExit()

        'MsgBox("procedo2")



        LogMsg("APERTURA CONNESSIONE CON DB METRONOMO...", True)
        Try
            cnMetronomo = GetMetronomoConnection()
            cnMetronomo.Open()
            LogMsg("OK", True)
        Catch ex As Exception
            'mecMisc.MsgErr(ex.Message())
            cnMetronomo = Nothing
            LogMsg("ERRORE - " & ex.Message)
        End Try

        If cnMetronomo Is Nothing Then
            If Not mecMisc.MsgQuest("Una o più connessioni non sono funzionanti continuare comunque?") Then
                Exit Sub
            Else
                DisableControl(btnEsegui)
            End If
        End If


        LogMsg("APERTURA CONNESSIONE CON DB IMPORT...", True)

        'Try
        '    cnImport = GetImportConnection()
        '    cnImport.Open()
        '    LogMsg("OK", True)
        'Catch ex As Exception
        '    mecMisc.MsgErr(ex.Message())
        '    cnImport = Nothing
        '    If cnMetronomo IsNot Nothing Then cnMetronomo.Close()
        '    LogMsg("ERRORE - " & ex.Message)
        'End Try


        LogMsg("CARICAMENTO TABELLE IN CORSO", True)

        'Carica tabelle Metronomo

        If cnMetronomo IsNot Nothing Then Carica_Metronomo()

        'Carica tabelle Import
        If Not onlyMet Then Carica_Import()

        SistemaTabelle()
        LogMsg("CARICAMENTO TABELLE TERMINATO", True)

    End Sub
    Sub Esegui()
        'dicNazioni = New Dictionary(Of String, String)
        'dicNazioni.Add("0", "")
        'For Each drn As DataRow In dsImport.Tables("Nazioni").Rows
        '    dicNazioni.Add(drn("id"), drn("nz_descri"))
        'Next

        Try
            For Each moItem In clbFunzioni.CheckedItems
                Select Case UCase(CStr(moItem))
                    'Case "UTENSILI"
                    '    Importa_Utensili()
                    'Case "LISTINO GENERALE"
                    '    Importa_listino_generale()
                    'Case "CLOUD IVA"
                    '    Importa_IVA_cloud()
                    'Case "CLOUD MODALITA' PAGAMENTO"
                    '    Importa_Pagamenti_cloud()
                    Case "IMPORT CONTATTI CLIENTI"
                        Importa_Clienti_Cloud()
                    Case "IMPORT CONTATTI FORNITORI"
                        Importa_Fornitori_Cloud()
                    'Case "CLOUD VETTORI"
                    '    Importa_vettori_Cloud()
                    'Case "IMPORTA CONTATTI"
                    '    Importa_Contatti()
                    'Case "IMPORTA ARTICOLI"
                    '    Importa_Articoli()
                    'Case "IMPORTA ORDINI TESTATE"
                    '    Importa_OrdiniProduzione_Testate()
                    'Case "IMPORTA ORDINI"
                    '    Importa_OrdiniProduzione()


                    Case "NUKE"
                        Nuke()
                    Case Else
                        LogMsg("FUNZIONE " & moItem & " NON TROVATA")
                End Select
                GC.Collect()
            Next

            LogMsg("OPERAZIONE COMPLETATA", True)


        Catch ex As Exception
            LogMsg("MASTER ERROR! " & vbCrLf & ex.Message, )
        End Try


    End Sub

    Sub SistemaTabelle()
        If Me.InvokeRequired Then
            Dim d As New GenericSubDelegate(AddressOf SistemaTabelle)
            Me.Invoke(d)
        Else
            lstTabelle.Items.Clear()
            Dim dtTemp As DataTable
            For Each dtTemp In dsImport.Tables
                Dim smoTemp(1) As String
                Dim moItem As New ListViewItem
                smoTemp(1) = "Import"
                smoTemp(0) = dtTemp.TableName
                moItem = New ListViewItem(smoTemp)
                AddTable(moItem)
            Next
            For Each dtTemp In dsMetronomo.Tables
                Dim smoTemp(1) As String
                Dim moItem As New ListViewItem
                smoTemp(1) = "Metronomo"
                smoTemp(0) = dtTemp.TableName
                moItem = New ListViewItem(smoTemp)
                AddTable(moItem)
            Next
        End If
    End Sub
    Sub SvuotaLista()
        If Me.InvokeRequired Then
            Dim d As New GenericSubDelegate(AddressOf SvuotaLista)
            Me.Invoke(d)
        Else
            lstTabelle.Items.Clear()
        End If
    End Sub

    Sub AddTable(ByVal sTabella As ListViewItem)
        If Me.InvokeRequired Then
            Dim d As New AddTable_Delegate(AddressOf AddTable)
            Me.Invoke(d, sTabella)
        Else
            Me.lstTabelle.Items.Add(sTabella)
        End If
    End Sub

    Function bCaricaSQL(ByVal sSql As String, ByVal sTableName As String, ByVal cnn As SqlClient.SqlConnection) As Boolean
        Try
            LogMsg("CARICAMENTO TABELLA " & sTableName & " IN CORSO...")
            da = New SqlClient.SqlDataAdapter(sSql, cnn)
            Dim dt As New DataTable(sTableName)
            da.Fill(dt)

            Dim sTemp(1) As String
            If cnn Is cnImport Then
                dsImport.Tables.Add(dt)
                sTemp(1) = "Import"
            Else
                dsMetronomo.Tables.Add(dt)
                sTemp(1) = "Metronomo"
            End If
            sTemp(0) = sTableName

            Dim moItem As New ListViewItem(sTemp)
            AddTable(moItem)
            LogMsg("CARICAMENTO TABELLA " & sTableName & " COMPLETATO")
            Return True
        Catch ex As Exception
            'mecMisc.MsgErr("Impossibile caricare la tabella " & sTableName & "!" & vbCrLf & ex.Message)
            LogMsg("Impossibile caricare la tabella " & sTableName & "!" & vbCrLf & ex.Message)
            Return False
        End Try

    End Function
    Function bCaricaSQL(ByVal sTableName As String) As Boolean
        Return bCaricaSQL("SELECT * FROM " & sTableName, sTableName, cnMetronomo)
    End Function
    Function bCaricaSQL(ByVal sTableName As String, ByVal cn As OleDb.OleDbConnection) As Boolean
        Return bCaricaSQL("SELECT * FROM " & sTableName, sTableName, cn)
    End Function
    Function bCaricaSQL(ByVal sSql As String, ByVal sTableName As String, ByVal cnn As OleDb.OleDbConnection) As Boolean
        Try
            LogMsg("CARICAMENTO TABELLA " & sTableName & " IN CORSO...")
            Dim da As New OleDb.OleDbDataAdapter(sSql, cnn)
            Dim dt As New DataTable(sTableName)
            da.Fill(dt)

            Dim sTemp(2) As String

            dsImport.Tables.Add(dt)
            sTemp(1) = "Import"

            sTemp(0) = sTableName
            sTemp(2) = dt.Rows.Count
            Dim moItem As New ListViewItem(sTemp)
            AddTable(moItem)
            LogMsg("CARICAMENTO TABELLA " & sTableName & " COMPLETATO")
            Return True
        Catch ex As Exception
            'mecMisc.MsgErr("Impossibile caricare la tabella " & sTableName & "!" & vbCrLf & ex.Message)
            LogMsg("Impossibile caricare la tabella " & sTableName & "!" & vbCrLf & ex.Message)
            Return False
        End Try

    End Function

    Function sGetNota(ByVal dr As DataRow) As String
        Dim sb As New System.Text.StringBuilder
        sb.Append("OPERATORE: " & dr("OPERATORE"))
        sb.Append(vbCrLf)
        sb.Append("DATA ULTIMA PRODUZIONE: " & dr("DATA"))
        sGetNota = sb.ToString
    End Function

    Function GetTableName(ByVal sTableName As String) As String
        Dim dt As DataTable
        Dim i As Integer = 0
        dt = dsImport.Tables(sTableName)
        While dt IsNot Nothing
            i += 1
            sTableName = sTableName & "(" & i & ")"
            dt = dsImport.Tables(sTableName)
        End While
        Return sTableName
    End Function

    Function Check_Query(Optional ByVal bEseguiSempre As Boolean = False, Optional ByVal iMaxQuery As Integer = 500) As Boolean
        If moQueryPool.Count > 0 And (bEseguiSempre Or moQueryPool.Count > iMaxQuery) Then
            If EseguiInserimento(moQueryPool) Then
                moQueryPool = New ArrayList
            Else
                'bBusy = False
                Return False
            End If
        End If
        Return True
    End Function

    Public Function EseguiInserimento(ByVal moQuery As ArrayList, Optional ByVal bDestinazioneImportDB As Boolean = False) As Boolean
        Dim bResult As Boolean = True
        Dim cnSQL As SqlClient.SqlConnection
        Dim sQuery As String = ""
        'If bDestinazioneImportDB Then
        '    cnSQL = GetImportConnection()
        'Else
        cnSQL = GetMetronomoConnection()
        'End If
        Dim tra As SqlClient.SqlTransaction = Nothing
        Dim k As Integer
        Try
            cnSQL.Open()
            tra = cnSQL.BeginTransaction
            Dim dbcmd As New SqlClient.SqlCommand
            dbcmd.Connection = cnSQL
            dbcmd.Transaction = tra

            k = 0
            For Each sQuery In moQuery
                k += 1
                Titolo("Esecuzione Query..." & k & "/" & moQuery.Count)
                'Debug.Print(sQuery)
                dbcmd.CommandText = sQuery
                dbcmd.ExecuteNonQuery()
                Progresso(k, moQuery.Count)
                'pgbProgresso.PerformStep()
            Next
            Progresso(0, moQuery.Count)
            tra.Commit()

            Titolo("")
        Catch ex As Exception
            If tra IsNot Nothing Then
                tra.Rollback()
            End If
            LogMsg(ex.Message & vbCrLf & sQuery)
            bResult = False
            Progresso(0, moQuery.Count)

        Finally
            cnSQL.Close()
        End Try
        Return bResult
    End Function
#End Region
#Region " Carica Metronomo - Import - Connessioni"
    Sub CompilaFunzioni()

        clbFunzioni.Items.Add("IMPORT CONTATTI CLIENTI", True)
        clbFunzioni.Items.Add("IMPORT CONTATTI FORNITORI", True)
        clbFunzioni.Items.Add("IMPORT DESTINAZIONI", True)
        clbFunzioni.Items.Add("IMPORT ARTICOLI", True)
        clbFunzioni.Items.Add("IMPORT LISTINI", True)

        clbFunzioni.Items.Add("EXPORT RIGHE DDT", False)


#If DEBUG Then
        clbFunzioni.Items.Add("NUKE", False)
#End If

    End Sub

    Function GetMetronomoConnection() As SqlClient.SqlConnection
        Dim cn As SqlClient.SqlConnection = Nothing
        Dim sb As New SqlClient.SqlConnectionStringBuilder
        sb.DataSource = txtMetronomoServer.Text
        sb.InitialCatalog = txtMetronomoDB.Text

        sb.UserID = txtMetronomoUser.Text
        sb.Password = txtMetronomoPassword.Text

        sb.ConnectTimeout = 5
        cn = New SqlClient.SqlConnection(sb.ToString)
        Return cn
    End Function
    Sub Carica_Metronomo()
        bCaricaSQL("CONTATTI")
        bCaricaSQL("ARTICOLI")
        '        bCaricaSQL("select [1],
        '[2],
        '[3],
        '[4],
        '[5],
        '[6],
        '[7],
        '[8],
        '[9],
        '[10],
        '[11],
        '[12],
        '[13],
        '[14],
        '[15],
        '[16],
        '[17],
        '[18],
        '[19],
        '[20],
        '[21],
        '[22],
        '[23],
        '[24],
        '[25],
        '[26],
        '[27],
        '[28],
        '[29],
        '[30]
        ' from (
        'select 
        'null as [1]
        ', null as [2]
        ', null as [3]
        ', null as [4]
        ', '001' as [5]
        ', null as [6]
        ', t1.Data as [7]
        ', null as [8]
        ', 'V' as [9]
        ', 'SPE' as [10]
        ', t1.IDDDT as [11]
        ', t1.data as [12]
        ', 'C' as [13]
        ', t3.Codice as [14]
        ', t4.Articolo [15]
        ', null as [16]
        ', '000001' as [17]
        ', t2.Quantità as [18]
        ', t2.Prezzo as [19]
        ', null as [20]
        ', null as [21]
        ', null as [22]
        ', LEFT(RTRIM(t2.descrizione),35) as [23] 
        ', '' as [24]
        ', null as [25]
        ', null as [26]
        ', null as [27]
        ', null as [28]
        ', null as [29]
        ', null as [30]
        ', t2.Posizione Posizione

        ' from ddt t1 left join
        '	DDT_DETTAGLIO t2 on t1.IDDDT= t2.IDDDT 
        '		left join contatti t3 on t1.IDContatto = t3.IDContatto
        '			left join ARTICOLI t4 on t2.IDArticolo = t4.IDArticolo
        'where 
        't1.Registrata is not null and 
        'month(t1.data)=month(getDate()) and year(t1.data)= year(getDate())


        'union all 

        'select 
        'null as [1]
        ', null as [2]
        ', null as [3]
        ', null as [4]
        ', '001' as [5]
        ', null as [6]
        ', t1.Data as [7]
        ', null as [8]
        ', 'C' as [9]
        ', 'SPE' as [10]
        ', t1.IDDDT as [11]
        ', t1.data as [12]
        ', 'C' as [13]
        ', t3.Codice as [14]
        ', null [15]
        ', null as [16]
        ', null as [17]
        ', null as [18]
        ', null as [19]
        ', null as [20]
        ', null as [21]
        ', null as [22]
        ', '' as [23]
        ', iif( 
        '	len(rtrim(t2.descrizione))-35 < 0
        '	, ''
        '	, Left(RIGHT(rtrim(t2.descrizione), len(rtrim(t2.descrizione))-35) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''),35)
        '	) as [24]
        ',  iif( 
        '	len(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''))-70 < 0
        '	, ''
        '	, left(RIGHT(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''), len(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''))-70) ,35)
        '	) as [25]
        ',  iif( 
        '	len(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''))-105 < 0
        '	, ''
        '	, left(RIGHT(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''), len(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''))-105) ,35)
        '	)  as [26]
        ', iif( 
        '	len(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''))-140 < 0
        '	, ''
        '	, left(RIGHT(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''), len(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''))-140) ,35)
        '	)  as [27]
        ',  iif( 
        '	len(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''))-175 < 0
        '	, ''
        '	, left(RIGHT(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''), len(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''))-175) ,35)
        '	)  as [28]
        ',  iif( 
        '	len(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''))-210 < 0
        '	, ''
        '	, left(RIGHT(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''), len(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''))-210) ,35)
        '	)  as [29]
        ',  iif( 
        '	len(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''))-245 < 0
        '	, ''
        '	, left(RIGHT(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''), len(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''))-245) ,35)
        '	)  as [30]
        ', t2.Posizione+1 Posizione

        ' from ddt t1 left join
        '	DDT_DETTAGLIO t2 on t1.IDDDT= t2.IDDDT 
        '		left join contatti t3 on t1.IDContatto = t3.IDContatto
        '			left join ARTICOLI t4 on t2.IDArticolo = t4.IDArticolo
        'where 
        ' t1.Registrata is not null and 
        'month(t1.data)=month(getDate()) and year(t1.data)= year(getDate()) 
        ') 
        'kk
        ' order by  [11], Posizione asc
        '", "DDT", cnMetronomo)

        bCaricaSQL("select 
[1],
[2],
[3],
[4],
[5],
[6],
[7],
[8],
[9],
[10],
[11],
[12],
[13],
[14],
[15],
[16],
[17],
[18],
[19],
[20],
[21],
[22],
[23],
[24],
[25],
[26],
[27],
[28],
[29],
[30],
[31]

 from (
select 
null as [1]
, null as [2]
, null as [3]
, null as [4]
, '001' as [5]
, null as [6]
, t1.Data as [7]
, null as [8]
, 'V' as [9]
, 'SPE' as [10]
, t1.IDDDT as [11]
, t1.data as [12]
, 'C' as [13]
, t3.Codice as [14]
, t4.Articolo [15]
, null as [16]
, '000001' as [17]
, t2.Quantità as [18]
, t2.Prezzo as [19]
, null as [20]
, null as [21]
, null as [22]
, LEFT(RTRIM(t2.descrizione),35) as [23] 
, '' as [24]
, null as [25]
, null as [26]
, null as [27]
, null as [28]
, null as [29]
, null as [30]
,  rtrim(t2.descrizione)+ ' '+coalesce(cast(t2.Nota as varchar(max)),'') as [31]
, t2.Posizione Posizione

 from ddt t1 left join
	DDT_DETTAGLIO t2 on t1.IDDDT= t2.IDDDT 
		left join contatti t3 on t1.IDContatto = t3.IDContatto
			left join ARTICOLI t4 on t2.IDArticolo = t4.IDArticolo
where 
 t1.StatusInterop = 0 and 
month(t1.data)=month(getDate()) and year(t1.data)= year(getDate())
and t1.Causale='VENDITA'

union all 

select 
null as [1]
, null as [2]
, null as [3]
, null as [4]
, '001' as [5]
, null as [6]
, t1.Data as [7]
, null as [8]
, 'C' as [9]
, 'SPE' as [10]
, t1.IDDDT as [11]
, t1.data as [12]
, 'C' as [13]
, t3.Codice as [14]
, null [15]
, null as [16]
, null as [17]
, null as [18]
, null as [19]
, null as [20]
, null as [21]
, null as [22]
, '' as [23]
, iif( 
	len(rtrim(t2.descrizione))-35 < 0
	, ''
	, Left(RIGHT(rtrim(t2.descrizione), len(rtrim(t2.descrizione))-35) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''),35)
	) as [24]
,  iif( 
	len(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''))-70 < 0
	, ''
	, left(RIGHT(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''), len(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''))-70) ,35)
	) as [25]
,  iif( 
	len(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''))-105 < 0
	, ''
	, left(RIGHT(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''), len(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''))-105) ,35)
	)  as [26]
, iif( 
	len(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''))-140 < 0
	, ''
	, left(RIGHT(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''), len(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''))-140) ,35)
	)  as [27]
,  iif( 
	len(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''))-175 < 0
	, ''
	, left(RIGHT(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''), len(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''))-175) ,35)
	)  as [28]
,  iif( 
	len(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''))-210 < 0
	, ''
	, left(RIGHT(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''), len(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''))-210) ,35)
	)  as [29]
,  iif( 
	len(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''))-245 < 0
	, ''
	, left(RIGHT(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''), len(rtrim(t2.descrizione) +' - ' +coalesce(cast(t2.Nota as varchar(max)),''))-245) ,35)
	)   as [30]
	, rtrim(t2.descrizione)+ ' '+coalesce(cast(t2.Nota as varchar(max)),'') as [31]
, t2.Posizione+1 Posizione

 from ddt t1 left join
	DDT_DETTAGLIO t2 on t1.IDDDT= t2.IDDDT 
		left join contatti t3 on t1.IDContatto = t3.IDContatto
			left join ARTICOLI t4 on t2.IDArticolo = t4.IDArticolo
where 
t1.StatusInterop = 0 and 
month(t1.data)=month(getDate()) and year(t1.data)= year(getDate()) 
and t1.Causale='VENDITA'
) 
kk


  order by  [11], Posizione asc", "DDT", cnMetronomo)
        '       bCaricaSQL("select idddt, data, DestinatarioRagioneSociale, StatusInterop from DDT where StatusInterop=1", "DDT_EXP", cnMetronomo)

    End Sub

    Function GetImportConnection() As OleDb.OleDbConnection
        GetImportConnection = New OleDb.OleDbConnection("Provider=MSDASQL.1;Persist Security Info=False;User ID=admin;Password=marlboro;Data Source=MECMATICA")
    End Function

    Sub Carica_Import()

        CaricaExcel(SurceFilesPath.Text & "\ANABZ.xls", "ART_1")
        CaricaExcel(SurceFilesPath.Text & "\ANABV.xls", "ART_2")
        CaricaExcel(SurceFilesPath.Text & "\ANABG.xls", "CLIENTI_1")
        CaricaExcel(SurceFilesPath.Text & "\ANABH.xls", "CLIENTI_2")
        CaricaExcel(SurceFilesPath.Text & "\ANABL.xls", "FORNITORI_1")
        CaricaExcel(SurceFilesPath.Text & "\ANABM.xls", "FORNITORI_2")

        'Dim c As Integer = 0
        'While System.IO.File.Exists(SurceFilesPath.Text & "\Destinazione clienti.xls") = False

        '    Threading.Thread.Sleep(1000)
        '    c += 1
        '    Titolo("attesa file " & c)
        'End While
        'CaricaExcel(SurceFilesPath.Text & "\Destinazione clienti.xls", "DESTINAZIONI")
        'CaricaExcel(SurceFilesPath.Text & "\Listino base.xls", "LISTINI")
    End Sub

#End Region
#Region "Funzioni "
#Region " Funzioni Importazione"
    Sub Importa_vettori_Cloud()
        LogMsg("IMPORTAZIONE CONTATTI FORNITORI...", True)
        Dim dt As DataTable
        moQueryPool = New ArrayList
        Dim dv As DataView
        Dim da As New SqlClient.SqlDataAdapter("SELECT MAX(IDContatto) as MAXI FROM CONTATTI", cnMetronomo)
        Dim da2 As SqlClient.SqlDataAdapter
        Dim idpagamento As Integer
        Dim dtTemp As New DataTable

        da.Fill(dtTemp)
        If dtTemp.Rows.Count > 0 Then
            lIDContatto = Val("" & dtTemp.Rows(0)(0))
        End If

        Dim i As Integer = 0
        Dim sTipo As String = ""

        LogMsg("IMPORTAZIONE CONTATTI...")
        i = 0


        dt = dsImport.Tables("VETTORI")
        For Each dr As DataRow In dt.Rows
            If Not (Trim(dr("ID")) = "" Or Trim(dr("ID")) = " ") Then
                idpagamento = vbNull
                i += 1
                Progresso(i, dt.Rows.Count)
                sTipo = ""

                dv = New DataView(dsMetronomo.Tables("CONTATTI"), "Codice = 'V" & dr("codiceext") & "'", "IDContatto", DataViewRowState.CurrentRows)
                If dv.Count = 0 Then
                    LogMsg("CODICE CONTATTO NON TROVATO")
                    AggiungiContattoVettore(dr, sTipo, idpagamento)
                Else
                    Dim bModified As Boolean = True

                    If bModified Then
                        AggiungiContattoVettore(dr, sTipo, idpagamento, True, dv(0)("IDContatto"))
                    End If

                End If

                If Not Check_Query() Then
                    Exit Sub
                End If
            End If
        Next
        Check_Query(True)

    End Sub

    Sub Importa_Fornitori_Cloud()
        LogMsg("IMPORTAZIONE CONTATTI ...", True)
        Dim dt As DataTable
        moQueryPool = New ArrayList
        Dim dv As DataView
        Dim da As New SqlClient.SqlDataAdapter("SELECT MAX(IDContatto) as MAXI FROM CONTATTI", cnMetronomo)
        Dim da2 As SqlClient.SqlDataAdapter
        Dim idpagamento As Integer
        Dim dtTemp As New DataTable

        da.Fill(dtTemp)
        If dtTemp.Rows.Count > 0 Then
            lIDContatto = Val("" & dtTemp.Rows(0)(0))
        End If


        Dim i As Integer = 0
        Dim sTipo As String = ""

        LogMsg("IMPORTAZIONE CONTATTI FORNITORI...")
        i = 0


        dt = dsImport.Tables("FORNITORI_1")
        For Each dr As DataRow In dt.Rows

            idpagamento = vbNull


            i += 1
            Progresso(i, dt.Rows.Count)
            sTipo = "F"
            dv = New DataView(dsMetronomo.Tables("CONTATTI"), "Codice = '" & sTipo & dr("BLCFOR") & "'", "IDContatto", DataViewRowState.CurrentRows)
            If dv.Count = 0 Then
                LogMsg("CODICE CONTATTO NON TROVATO")
                AggiungiContattoFornitore(dr, sTipo, idpagamento)
            Else
                Dim bModified As Boolean = True
                If bModified Then
                    AggiungiContattoFornitore(dr, sTipo, idpagamento, True, dv(0)("IDContatto"))
                End If
            End If

            If Not Check_Query() Then
                Exit Sub
            End If
        Next
        Check_Query(True)

    End Sub

    Sub Importa_Clienti_Cloud()
        LogMsg("IMPORTAZIONE CONTATTI ...", True)
        Dim dt As DataTable
        moQueryPool = New ArrayList
        Dim dv As DataView
        Dim da As New SqlClient.SqlDataAdapter("SELECT MAX(IDContatto) as MAXI FROM CONTATTI", cnMetronomo)
        Dim da2 As SqlClient.SqlDataAdapter
        Dim idpagamento As Integer
        Dim dtTemp As New DataTable

        da.Fill(dtTemp)
        If dtTemp.Rows.Count > 0 Then
            lIDContatto = Val("" & dtTemp.Rows(0)(0))
        End If


        Dim i As Integer = 0
        Dim sTipo As String = ""

        LogMsg("IMPORTAZIONE CONTATTI (CLOUD CLIENTI)...")
        i = 0


        dt = dsImport.Tables("CLIENTI_1")
        For Each dr As DataRow In dt.Rows

            idpagamento = vbNull


            i += 1
            Progresso(i, dt.Rows.Count)
            sTipo = "C"
            dv = New DataView(dsMetronomo.Tables("CONTATTI"), "Codice = '" & dr("BGCCLI") & "'", "IDContatto", DataViewRowState.CurrentRows)
            If dv.Count = 0 Then
                LogMsg("CODICE CONTATTO NON TROVATO")
                AggiungiContatto(dr, sTipo, idpagamento)
            Else
                Dim bModified As Boolean = True
                If bModified Then
                    AggiungiContatto(dr, sTipo, idpagamento, True, dv(0)("IDContatto"))
                End If
            End If

            If Not Check_Query() Then
                Exit Sub
            End If
        Next
        Check_Query(True)
    End Sub
    Sub Importa_Pagamenti_cloud()
        LogMsg("IMPORTAZIONE PAGAMENTI CLOUD", True)
        Dim dt As DataTable
        moQueryPool = New ArrayList
        Dim dv As DataView
        Dim da As New SqlClient.SqlDataAdapter("SELECT MAX(IDPagamento) as MAXI FROM TIPO_PAGAMENTO", cnMetronomo)
        Dim dtTemp As New DataTable

        da.Fill(dtTemp)
        If dtTemp.Rows.Count > 0 Then
            lIDContatto = Val("" & dtTemp.Rows(0)(0))
        End If


        Dim i As Integer = 0
        Dim sTipo As String = ""

        LogMsg("IMPORTAZIONE PAGAMENTI...")
        i = 0

        dt = dsImport.Tables("Pagamenti")
        For Each dr As DataRow In dt.Rows
            i += 1
            Progresso(i, dt.Rows.Count)

            sTipo = ""


            dv = New DataView(dsMetronomo.Tables("TIPO_PAGAMENTO"), "PAGAMENTO = '" & dr("id") & "'", "PAGAMENTO", DataViewRowState.CurrentRows)
            If dv.Count = 0 Then
                LogMsg("CODICE PAGAMENTO NON TROVATO")
                AggiungiPagamento(dr, sTipo)
            Else
                Dim bModified As Boolean = True

                If bModified Then
                    AggiungiPagamento(dr, sTipo, True, dv(0)("IDPagamento"))
                End If

            End If

            If Not Check_Query() Then
                Exit Sub
            End If
        Next
        Check_Query(True)

    End Sub

    Sub Importa_IVA_cloud()
        LogMsg("IMPORTAZIONE IVA CLOUD", True)
        Dim dt As DataTable
        moQueryPool = New ArrayList
        Dim dv As DataView

        Dim i As Integer = 0
        Dim sTipo As String = ""

        LogMsg("IMPORTAZIONE ALIQUOTE IVA...")
        i = 0

        dt = dsImport.Tables("Iva")
        For Each dr As DataRow In dt.Rows
            i += 1
            Progresso(i, dt.Rows.Count)
            sTipo = ""


            dv = New DataView(dsMetronomo.Tables("ALIQUOTE_IVA"), "IDIva = '" & dr("iv_codice") & "'", "IDIva", DataViewRowState.CurrentRows)
            If dv.Count = 0 Then
                LogMsg("CODICE IVA NON TROVATO")
                AggiungiIVA(dr, sTipo)
            Else
                Dim bModified As Boolean = True

                If bModified Then
                    AggiungiIVA(dr, sTipo, True)
                End If

            End If

            If Not Check_Query() Then
                Exit Sub
            End If
        Next
        Check_Query(True)
    End Sub
    ' Importazione one shot.. qui per info storica...

    'Sub Importa_listino_generale()
    '    LogMsg("IMPORTAZIONE LISTINO PREZZI ARTICOLI...", True)
    '    Dim dt As DataTable
    '    moQueryPool = New ArrayList
    '    Dim dv As DataView
    '    Dim da As New SqlClient.SqlDataAdapter("SELECT MAX(IDLISTINODETTAGLIO) as MAXI FROM LISTINI_DETTAGLIO", cnMetronomo)
    '    Dim da2 As SqlClient.SqlDataAdapter
    '    Dim idpagamento As Integer
    '    Dim dtTemp As New DataTable

    '    da.Fill(dtTemp)
    '    If dtTemp.Rows.Count > 0 Then
    '        lIDContatto = Val("" & dtTemp.Rows(0)(0))
    '    End If


    '    Dim i As Integer = 0
    '    Dim sTipo As String = ""

    '    LogMsg("IMPORTAZIONE LISTINI...")
    '    i = 0


    '    dt = dsImport.Tables("Listini_DETTAGLIO")
    '    For Each dr As DataRow In dt.Rows

    '        LogMsg("INSERIMENTO LISTINO DETTAGLIO ")
    '        lIDContatto = lIDContatto + 1
    '        AggiungiListinoDettaglio(dr)


    '        If Not Check_Query() Then
    '            Exit Sub
    '        End If
    '    Next
    '    Check_Query(True)

    'End Sub
    'Sub Importa_Contatti()
    '    LogMsg("IMPORTAZIONE CONTATTI...", True)
    '    Dim dt As DataTable
    '    moQueryPool = New ArrayList
    '    Dim dv As DataView
    '    Dim da As New SqlClient.SqlDataAdapter("SELECT MAX(IDContatto) as MAXI FROM CONTATTI", cnMetronomo)
    '    Dim dtTemp As New DataTable

    '    da.Fill(dtTemp)
    '    If dtTemp.Rows.Count > 0 Then
    '        lIDContatto = Val("" & dtTemp.Rows(0)(0))
    '    End If

    '    Dim i As Integer = 0
    '    Dim sTipo As String = ""

    '    LogMsg("IMPORTAZIONE CONTATTI...")
    '    i = 0

    '    dt = dsImport.Tables("anagrafica")
    '    For Each dr As DataRow In dt.Rows
    '        i += 1
    '        Progresso(i, dt.Rows.Count)

    '        sTipo = "" & dr("anagrafica_codeanagraficatipo")


    '        dv = New DataView(dsMetronomo.Tables("CONTATTI"), "Codice = '" & dr("codiceext") & "'", "IDContatto", DataViewRowState.CurrentRows)
    '        If dv.Count = 0 Then
    '            LogMsg("CODICE CONTATTO NON TROVATO")
    '            AggiungiContatto(dr, sTipo, 0)
    '        Else
    '            Dim bModified As Boolean = True

    '            If bModified Then
    '                AggiungiContatto(dr, sTipo, 0, True, dv(0)("IDContatto"))
    '            End If

    '        End If

    '        If Not Check_Query() Then
    '            Exit Sub
    '        End If
    '    Next
    '    Check_Query(True)

    'End Sub
    ' aggiornamento 2/5/2017 one shot -  lasciare per storico
    'Sub Importa_Utensili()
    '    LogMsg("IMPORTAZIONE UTENSILI...", True)
    '    Dim dt As DataTable = dsImport.Tables("UTENSILI") ' cambio per importazione one shot
    '    moQueryPool = New ArrayList
    '    'Dim sCodiceCmp As String
    '    lIDFase = 4540
    '    lIDCiclo = 2629
    '    Dim da As New SqlClient.SqlDataAdapter("SELECT MAX(IDArticolo) as MAXI FROM ARTICOLI", cnMetronomo)
    '    Dim dtTemp As New DataTable
    '    da.Fill(dtTemp)
    '    If dtTemp.Rows.Count > 0 Then
    '        lIDArticolo = Val("" & dtTemp.Rows(0)(0))
    '    End If
    '    Dim sArticoloLocal As String
    '    Dim bUpdate As Boolean
    '    Dim i As Integer = 0
    '    LogMsg("IMPORTAZIONE UTENSILI...")
    '    For Each dr As DataRow In dt.Rows
    '        i += 1
    '        Progresso(i, dt.Rows.Count)

    '        bUpdate = False

    '        sArticoloLocal = "" & dr("prodotti_codice")

    '        If sArticoloLocal = "R00000433" Then Beep()

    '        If bArticoloExists(sArticoloLocal, , bUpdate) = False Then
    '            AggiungiUtensile(dr, bUpdate)
    '        Else
    '            AggiungiUtensile(dr, bUpdate)
    '            '    AggiungiArticolo(dr, True)
    '        End If



    '        'aggiungo il listino
    '        'AggiungiListino(dr, sArticoloLocal)

    '        Progresso(i, dt.Rows.Count)
    '        If Not Check_Query() Then
    '            Exit Sub
    '        End If
    '    Next
    '    Check_Query(True)
    'End Sub

    'Sub Importa_Articoli()
    '    LogMsg("IMPORTAZIONE ARTICOLI...", True)
    '    Dim dt As DataTable = dsImport.Tables("ARTICOLI") ' cambio per importazione one shot
    '    moQueryPool = New ArrayList
    '    'Dim sCodiceCmp As String
    '    lIDFase = 4540
    '    lIDCiclo = 2629
    '    Dim da As New SqlClient.SqlDataAdapter("SELECT MAX(IDArticolo) as MAXI FROM ARTICOLI", cnMetronomo)
    '    Dim dtTemp As New DataTable
    '    da.Fill(dtTemp)
    '    If dtTemp.Rows.Count > 0 Then
    '        lIDArticolo = Val("" & dtTemp.Rows(0)(0))
    '    End If
    '    Dim sArticoloLocal As String
    '    Dim bUpdate As Boolean
    '    Dim i As Integer = 0
    '    LogMsg("IMPORTAZIONE ARTICOLI...")
    '    For Each dr As DataRow In dt.Rows
    '        i += 1
    '        Progresso(i, dt.Rows.Count)

    '        bUpdate = False

    '        sArticoloLocal = "" & dr("prodotti_codice")

    '        If sArticoloLocal = "R00000433" Then Beep()

    '        If bArticoloExists(sArticoloLocal, , bUpdate) = False Then
    '            AggiungiArticolo(dr, bUpdate)
    '        Else
    '            '    AggiungiArticolo(dr, True)
    '        End If



    '        'aggiungo il listino
    '        'AggiungiListino(dr, sArticoloLocal)

    '        Progresso(i, dt.Rows.Count)
    '        If Not Check_Query() Then
    '            Exit Sub
    '        End If
    '    Next
    '    Check_Query(True)
    'End Sub
    'Sub Importa_OrdiniProduzione_Testate()
    '    LogMsg("IMPORTAZIONE ORDINI...", True)
    '    Dim dt As DataTable = dsImport.Tables("DOCUMENTI")
    '    moQueryPool = New ArrayList
    '    Dim moSQL As New MecmaticaSql.Sql

    '    Dim i As Integer = 0
    '    LogMsg("IMPORTAZIONE ORDINI CLIENTI TESTATE ...")
    '    For Each dr As DataRow In dt.Rows
    '        i += 1
    '        Progresso(i, dt.Rows.Count)
    '        If bOrdineTestateExist(dr("documenti_id")) = False Then
    '            AggiungiOrdineProduzioneTestate(dr)
    '        Else
    '            AggiungiOrdineProduzioneTestate(dr, True)
    '        End If

    '        If Not Check_Query() Then
    '            Exit Sub
    '        End If
    '    Next
    '    Check_Query(True)

    '    'controllo ordini aperti in metronomo se esistono ancora nell'import
    '    'in caso contrario evado manualmente come IMPORT
    '    dt = dsMetronomo.Tables("ORDINI")
    '    For Each dr As DataRow In dt.Rows
    '        If dr("Evaso") Then
    '            'ordine già evaso
    '        Else
    '            'ordine da evadere, controllo con ordini da importare
    '            If Not bOrdineImportExist(dr("IDORDINE")) Then
    '                moSQL = New MecmaticaSql.Sql
    '                With moSQL
    '                    .Table = "ORDINI_CLIENTI"
    '                    .Where = "IDOrdine=" & dr("IDORDINE")
    '                    .AddField(, "Evaso", MecmaticaSql.Sql.eFieldType.tBOOLEAN, 1)
    '                    .AddField(, "EvasoManuale", MecmaticaSql.Sql.eFieldType.tBOOLEAN, 1)
    '                    .AddField(, "DataEvasione", MecmaticaSql.Sql.eFieldType.tDATETIME, Now)
    '                    .AddField(, "EvasoDa", MecmaticaSql.Sql.eFieldType.tCHAR, "IMPORT")
    '                End With
    '                LogMsg("ORDINE " & dr("IDOrdine") & " EVASO!")
    '                moQueryPool.Add(moSQL.SqlUpdate)
    '            End If

    '        End If
    '    Next
    '    Check_Query(True)
    'End Sub

    'Sub Importa_OrdiniProduzione()
    '    LogMsg("IMPORTAZIONE ORDINI...", True)
    '    Dim dt As DataTable = dsImport.Tables("ORDINI")
    '    moQueryPool = New ArrayList
    '    Dim moSQL As New MecmaticaSql.Sql

    '    Dim i As Integer = 0
    '    LogMsg("IMPORTAZIONE ORDINI ...")
    '    For Each dr As DataRow In dt.Rows
    '        i += 1
    '        Progresso(i, dt.Rows.Count)
    '        If bOrdineExist(dr("documentiprodotti_id")) = False Then
    '            AggiungiOrdineProduzione(dr)
    '        Else
    '            AggiungiOrdineProduzione(dr, True)
    '        End If

    '        If Not Check_Query() Then
    '            Exit Sub
    '        End If
    '    Next
    '    Check_Query(True)

    '    'controllo ordini aperti in metronomo se esistono ancora nell'import
    '    'in caso contrario evado manualmente come IMPORT
    '    dt = dsMetronomo.Tables("ORDINI")
    '    For Each dr As DataRow In dt.Rows
    '        If dr("Evaso") Then
    '            'ordine già evaso
    '        Else
    '            'ordine da evadere, controllo con ordini da importare
    '            If Not bOrdineImportExist(dr("IDORDINE")) Then
    '                moSQL = New MecmaticaSql.Sql
    '                With moSQL
    '                    .Table = "ORDINI"
    '                    .Where = "IDOrdine=" & dr("IDORDINE")
    '                    .AddField(, "Evaso", MecmaticaSql.Sql.eFieldType.tBOOLEAN, 1)
    '                    .AddField(, "EvasoManuale", MecmaticaSql.Sql.eFieldType.tBOOLEAN, 1)
    '                    .AddField(, "DataEvasione", MecmaticaSql.Sql.eFieldType.tDATETIME, Now)
    '                    .AddField(, "EvasoDa", MecmaticaSql.Sql.eFieldType.tCHAR, "IMPORT")
    '                End With
    '                LogMsg("ORDINE " & dr("IDOrdine") & " EVASO!")
    '                moQueryPool.Add(moSQL.SqlUpdate)
    '            End If

    '        End If
    '    Next
    '    Check_Query(True)
    'End Sub
#End Region
#Region " Funzioni Aggiungi"
    Sub AggiungiIVA(ByVal dr As DataRow, ByVal sTipo As String, Optional ByVal bUpdate As Boolean = False)
        Dim moSql As New MecmaticaSql.Sql

        moSql.Table = "ALIQUOTE_IVA"
        moSql.AddField(, "IDIVA", MecmaticaSql.Sql.eFieldType.tCHAR, dr("iv_codice"), , True, False)
        moSql.AddField(, "DESCRIZIONE", MecmaticaSql.Sql.eFieldType.tCHAR, "" & dr("iv_descri"))
        moSql.AddField(, "PERCENTUALE", MecmaticaSql.Sql.eFieldType.tNUMERIC, "" & dr("iv_valore"))
        moSql.AddField(, "INDETRAIBILE", MecmaticaSql.Sql.eFieldType.tNUMERIC, "" & dr("iv_indetra"))

        If bUpdate Then
            moSql.Where = "IDIVA = '" & dr("iv_codice") & "'"
            LogMsg("AGGIORNATA ALIQUTA IVA " & dr("iv_codice"))
            moQueryPool.Add(moSql.SqlUpdate)
        Else
            LogMsg("AGGIUNTA ALIQUOTA IVA " & dr("iv_codice"))
            moQueryPool.Add(moSql.SqlInsert)
        End If

    End Sub
    Sub AggiungiPagamento(ByVal dr As DataRow, ByVal sTipo As String, Optional ByVal bUpdate As Boolean = False, Optional ByVal lIDContattoUpdate As Long = 0)
        Dim moSql As New MecmaticaSql.Sql

        '++ Note ++
        '-- Da File Specifiche enum tipo pagamento di Alfaweb
        Dim dict As New Dictionary(Of String, String)()
        dict.Add("0", "CONTANTI")
        dict.Add("1", "RID")
        dict.Add("2", "Bonifico Bancario")
        dict.Add("3", "Riba")
        dict.Add("4", "Carta di credito o Bancomat")
        dict.Add("5", "Addebito")
        dict.Add("6", "Remittance")
        dict.Add("7", "Rimessa Diretta")
        dict.Add("8", "Accredito")
        dict.Add("9", "Cambiale")
        dict.Add("10", "Insoluti - Protesti")
        '++ End Note ++
        moSql.Table = "TIPO_PAGAMENTO"
        moSql.AddField(, "PAGAMENTO", MecmaticaSql.Sql.eFieldType.tCHAR, "" & dr("id"))
        moSql.AddField(, "DESCRIZIONE", MecmaticaSql.Sql.eFieldType.tCHAR, "" & dr("pa_descri"))
        moSql.AddField(, "FORMAPAGAMENTO", MecmaticaSql.Sql.eFieldType.tCHAR, "" & dict(dr("pa_tippagt")))

        If bUpdate Then
            moSql.Where = "IDPAGAMENTO = " & lIDContattoUpdate
            LogMsg("AGGIORNATO PAGAMENTO " & lIDContattoUpdate)
            moQueryPool.Add(moSql.SqlUpdate)
        Else
            lIDContatto += 1
            moSql.AddField(, "IDPAGAMENTO", MecmaticaSql.Sql.eFieldType.tNUMERIC, lIDContatto, True)
            LogMsg("AGGIUNTO PAGAMENTO " & lIDContatto)
            moQueryPool.Add(moSql.SqlInsert)
        End If
    End Sub

    Sub AggiungiContattoVettore(ByVal dr As DataRow, ByVal sTipo As String, ByVal idpagamento As Integer, Optional ByVal bUpdate As Boolean = False, Optional ByVal lIDContattoUpdate As Long = 0)
        Dim codiceext As String
        If dr("codiceext") = "" Then
            codiceext = "0" & dr("id")
        Else
            codiceext = dr("codiceext")
        End If
        Dim moSql As New MecmaticaSql.Sql
        moSql.Table = "CONTATTI"
        moSql.AddField(, "CODICE", MecmaticaSql.Sql.eFieldType.tCHAR, "V" & codiceext, , True, False)
        moSql.AddField(, "DESCRIZIONE", MecmaticaSql.Sql.eFieldType.tCHAR, "" & dr("ve_descri"))
        moSql.AddField(, "INDIRIZZO", MecmaticaSql.Sql.eFieldType.tCHAR, dr("ve_indiri"))
        moSql.AddField(, "CAP", MecmaticaSql.Sql.eFieldType.tCHAR, dr("ve___cap"))
        moSql.AddField(, "città", MecmaticaSql.Sql.eFieldType.tCHAR, dr("ve_locali"))
        moSql.AddField(, "PROVINCIA", MecmaticaSql.Sql.eFieldType.tCHAR, dr("ve_provin"))
        moSql.AddField(, "BVETTORE", MecmaticaSql.Sql.eFieldType.tBOOLEAN, True, , , False)
        moSql.AddField(, "PARTITAIVA", MecmaticaSql.Sql.eFieldType.tCHAR, dr("ve_piva"))
        moSql.AddField(, "TELEFONO", MecmaticaSql.Sql.eFieldType.tCHAR, dr("ve_telefono"))
        If bUpdate Then
            moSql.Where = "IDContatto = " & lIDContattoUpdate
            LogMsg("AGGIORNATO CONTATTO " & lIDContattoUpdate)
            moQueryPool.Add(moSql.SqlUpdate)
        Else
            lIDContatto += 1
            moSql.AddField(, "IDCONTATTO", MecmaticaSql.Sql.eFieldType.tNUMERIC, lIDContatto, True)
            LogMsg("AGGIUNTO CONTATTO " & lIDContatto)
            moQueryPool.Add(moSql.SqlInsert)
        End If
    End Sub
    Sub AggiungiListinoDettaglio(ByVal dr As DataRow)
        Dim moSql As New MecmaticaSql.Sql
        moSql.Table = "LISTINI_DETTAGLIO"
        moSql.AddField(, "IDLISTINODETTAGLIO", MecmaticaSql.Sql.eFieldType.tNUMERIC, lIDContatto, True)
        moSql.AddField(, "IDLISTINO", MecmaticaSql.Sql.eFieldType.tCHAR, "Generale", , True, False)
        moSql.AddField(, "IDARTICOLO", MecmaticaSql.Sql.eFieldType.tNUMERIC, translateArtCodeMySQL(dr("prodotti_codice")))
        moSql.AddField(, "QUANTITA", MecmaticaSql.Sql.eFieldType.tNUMERIC, 1)
        moSql.AddField(, "VALORE", MecmaticaSql.Sql.eFieldType.tNUMERIC, dr(1))
        moSql.AddField(, "VALORESCONTATO", MecmaticaSql.Sql.eFieldType.tNUMERIC, dr(1))
        moSql.AddField(, "APPLICASCONTOMANUALE", MecmaticaSql.Sql.eFieldType.tNUMERIC, 0)
        moSql.AddField(, "SCONTO", MecmaticaSql.Sql.eFieldType.tNUMERIC, 0)
        moSql.AddField(, "SCONTO2", MecmaticaSql.Sql.eFieldType.tNUMERIC, 0)
        moSql.AddField(, "VALIDITADATAINIZIO", MecmaticaSql.Sql.eFieldType.tDATETIME, New DateTime(2017, 1, 1))
        moSql.AddField(, "VALIDITADATAFINE", MecmaticaSql.Sql.eFieldType.tDATETIME, New DateTime(2020, 12, 31))
        moQueryPool.Add(moSql.SqlInsert)

    End Sub
    'Sub AggiungiContattoFornitore(ByVal dr As DataRow, ByVal sTipo As String, ByVal idpagamento As Integer, Optional ByVal bUpdate As Boolean = False, Optional ByVal lIDContattoUpdate As Long = 0)
    '    Dim codiceext As String = "F" & Strings.Right("0000" & dr("id"), 4)
    '    Dim moSql As New MecmaticaSql.Sql
    '    moSql.Table = "CONTATTI"
    '    moSql.AddField(, "CODICE", MecmaticaSql.Sql.eFieldType.tCHAR, codiceext, , True, False)
    '    moSql.AddField(, "DESCRIZIONE", MecmaticaSql.Sql.eFieldType.tCHAR, "" & dr("fo_ragsoc"))
    '    moSql.AddField(, "INDIRIZZO", MecmaticaSql.Sql.eFieldType.tCHAR, dr("fo_indiri"))
    '    moSql.AddField(, "CAP", MecmaticaSql.Sql.eFieldType.tCHAR, dr("fo___cap"))
    '    moSql.AddField(, "città", MecmaticaSql.Sql.eFieldType.tCHAR, dr("fo_locali"))
    '    moSql.AddField(, "PROVINCIA", MecmaticaSql.Sql.eFieldType.tCHAR, dr("fo_provin"))
    '    moSql.AddField(, "NAZIONE", MecmaticaSql.Sql.eFieldType.tCHAR, dicNazioni(dr("id_nazion")))
    '    moSql.AddField(, "NOTECOMMERCIALI", MecmaticaSql.Sql.eFieldType.tCHAR, dr("codiceext"))
    '    moSql.AddField(, "BFORNITORE", MecmaticaSql.Sql.eFieldType.tBOOLEAN, True, , , False)
    '    moSql.AddField(, "CF", MecmaticaSql.Sql.eFieldType.tCHAR, dr("Fo_codfis"))
    '    moSql.AddField(, "PARTITAIVA", MecmaticaSql.Sql.eFieldType.tCHAR, dr("fo_pariva"))
    '    If idpagamento <> vbNull And idpagamento > 0 Then
    '        moSql.AddField(, "IDPAGAMENTOCREDITO", MecmaticaSql.Sql.eFieldType.tCHAR, idpagamento)
    '        moSql.AddField(, "IDPAGAMENTODEBITO", MecmaticaSql.Sql.eFieldType.tCHAR, idpagamento)
    '    End If
    '    moSql.AddField(, "TELEFONO", MecmaticaSql.Sql.eFieldType.tCHAR, dr("fo_numtel"))
    '    moSql.AddField(, "FAX", MecmaticaSql.Sql.eFieldType.tCHAR, dr("fo_numfax"))
    '    moSql.AddField(, "WEBSITE", MecmaticaSql.Sql.eFieldType.tCHAR, dr("fo_indweb"))
    '    If bUpdate Then
    '        moSql.Where = "IDContatto = " & lIDContattoUpdate
    '        LogMsg("AGGIORNATO CONTATTO " & lIDContattoUpdate)
    '        moQueryPool.Add(moSql.SqlUpdate)
    '    Else
    '        lIDContatto += 1
    '        moSql.AddField(, "IDCONTATTO", MecmaticaSql.Sql.eFieldType.tNUMERIC, lIDContatto, True)
    '        LogMsg("AGGIUNTO CONTATTO " & lIDContatto)
    '        moQueryPool.Add(moSql.SqlInsert)
    '    End If
    'End Sub
    Sub AggiungiContattoFORNITORE(ByVal dr As DataRow, ByVal sTipo As String, ByVal idpagamento As Integer, Optional ByVal bUpdate As Boolean = False, Optional ByVal lIDContattoUpdate As Long = 0)

        Dim codiceext As String = "F" + dr("BLCFOR")
        Dim dre As DataRowView = getImportSupplyerExt(codiceext)
        Dim moSql As New MecmaticaSql.Sql

        moSql.Table = "CONTATTI"
        moSql.AddField(, "CODICE", MecmaticaSql.Sql.eFieldType.tCHAR, codiceext)
        moSql.AddField(, "DESCRIZIONE", MecmaticaSql.Sql.eFieldType.tCHAR, "" & dr("BLRAGS"))
        moSql.AddField(, "INDIRIZZO", MecmaticaSql.Sql.eFieldType.tCHAR, dr("BLINDI"))
        moSql.AddField(, "CAP", MecmaticaSql.Sql.eFieldType.tCHAR, dr("BLCCAP"))
        moSql.AddField(, "città", MecmaticaSql.Sql.eFieldType.tCHAR, dr("BLDLOC"))
        moSql.AddField(, "PROVINCIA", MecmaticaSql.Sql.eFieldType.tCHAR, dr("BLDPRO"))
        moSql.AddField(, "NAZIONE", MecmaticaSql.Sql.eFieldType.tCHAR, dr("BLCNAZ"))
        moSql.AddField(, "BCLIENTE", MecmaticaSql.Sql.eFieldType.tBOOLEAN, False)
        moSql.AddField(, "BFORNITORE", MecmaticaSql.Sql.eFieldType.tBOOLEAN, True)
        moSql.AddField(, "CF", MecmaticaSql.Sql.eFieldType.tCHAR, dr("BLCFIS"))
        moSql.AddField(, "PARTITAIVA", MecmaticaSql.Sql.eFieldType.tCHAR, dr("BLPIVA"))
        If idpagamento <> vbNull Or idpagamento > 0 Then
            moSql.AddField(, "IDPAGAMENTOCREDITO", MecmaticaSql.Sql.eFieldType.tCHAR, idpagamento)
            moSql.AddField(, "IDPAGAMENTODEBITO", MecmaticaSql.Sql.eFieldType.tCHAR, idpagamento)
        End If

        moSql.AddField(, "TELEFONO", MecmaticaSql.Sql.eFieldType.tCHAR, dr("BLCTEL"))
        moSql.AddField(, "FAX", MecmaticaSql.Sql.eFieldType.tCHAR, dr("BLNFAX"))
        '  moSql.AddField(, "WEBSITE", MecmaticaSql.Sql.eFieldType.tCHAR, dr("co_indweb"))

        If bUpdate Then
            moSql.Where = "IDContatto = " & lIDContattoUpdate
            LogMsg("AGGIORNATO CONTATTO " & lIDContattoUpdate)
            moQueryPool.Add(moSql.SqlUpdate)
        Else
            lIDContatto += 1
            moSql.AddField(, "IDCONTATTO", MecmaticaSql.Sql.eFieldType.tNUMERIC, lIDContatto, True)
            LogMsg("AGGIUNTO CONTATTO " & lIDContatto)
            moQueryPool.Add(moSql.SqlInsert)
        End If

        If Not bUpdate Then
            Dim drNew As DataRow
            drNew = dsMetronomo.Tables("CONTATTI").NewRow
            drNew("IDContatto") = lIDContatto
            drNew("CODICE") = "" & codiceext
            '  drNew("DESCRIZIONE") = "" & dr("co_ragsoc")
            dsMetronomo.Tables("CONTATTI").Rows.Add(drNew)
        End If

    End Sub
    Sub AggiungiContatto(ByVal dr As DataRow, ByVal sTipo As String, ByVal idpagamento As Integer, Optional ByVal bUpdate As Boolean = False, Optional ByVal lIDContattoUpdate As Long = 0)

        Dim codiceext As String = dr("BGCCLI")
        Dim moSql As New MecmaticaSql.Sql
        Dim dre As DataRowView = getImportCustomerExt(dr("BGCCLI"))
        moSql.Table = "CONTATTI"
        moSql.AddField(, "CODICE", MecmaticaSql.Sql.eFieldType.tCHAR, codiceext)
        moSql.AddField(, "DESCRIZIONE", MecmaticaSql.Sql.eFieldType.tCHAR, "" & dr("BGRAGS"))
        moSql.AddField(, "INDIRIZZO", MecmaticaSql.Sql.eFieldType.tCHAR, dr("BGINDI"))
        moSql.AddField(, "CAP", MecmaticaSql.Sql.eFieldType.tCHAR, dr("BGCCAP"))
        moSql.AddField(, "città", MecmaticaSql.Sql.eFieldType.tCHAR, dr("BGDLOC"))
        moSql.AddField(, "PROVINCIA", MecmaticaSql.Sql.eFieldType.tCHAR, dr("BGDPRV"))
        moSql.AddField(, "NAZIONE", MecmaticaSql.Sql.eFieldType.tCHAR,dr("BGCNAZ"))
        moSql.AddField(, "BCLIENTE", MecmaticaSql.Sql.eFieldType.tBOOLEAN, True)
        moSql.AddField(, "CF", MecmaticaSql.Sql.eFieldType.tCHAR, dr("BGCFIS"))
        moSql.AddField(, "PARTITAIVA", MecmaticaSql.Sql.eFieldType.tCHAR, dr("BGPIVA"))
        If idpagamento <> vbNull Or idpagamento > 0 Then
            moSql.AddField(, "IDPAGAMENTOCREDITO", MecmaticaSql.Sql.eFieldType.tCHAR, idpagamento)
            moSql.AddField(, "IDPAGAMENTODEBITO", MecmaticaSql.Sql.eFieldType.tCHAR, idpagamento)
        End If

        moSql.AddField(, "TELEFONO", MecmaticaSql.Sql.eFieldType.tCHAR, dr("BGTELE"))
        moSql.AddField(, "FAX", MecmaticaSql.Sql.eFieldType.tCHAR, dr("BGNFAX"))
        '  moSql.AddField(, "WEBSITE", MecmaticaSql.Sql.eFieldType.tCHAR, dr("co_indweb"))

        If bUpdate Then
            moSql.Where = "IDContatto = " & lIDContattoUpdate
            LogMsg("AGGIORNATO CONTATTO " & lIDContattoUpdate)
            moQueryPool.Add(moSql.SqlUpdate)
        Else
            lIDContatto += 1
            moSql.AddField(, "IDCONTATTO", MecmaticaSql.Sql.eFieldType.tNUMERIC, lIDContatto, True)
            LogMsg("AGGIUNTO CONTATTO " & lIDContatto)
            moQueryPool.Add(moSql.SqlInsert)
        End If

        If Not bUpdate Then
            Dim drNew As DataRow
            drNew = dsMetronomo.Tables("CONTATTI").NewRow
            drNew("IDContatto") = lIDContatto
            drNew("CODICE") = "" & codiceext
            '  drNew("DESCRIZIONE") = "" & dr("co_ragsoc")
            dsMetronomo.Tables("CONTATTI").Rows.Add(drNew)
        End If

    End Sub
    Sub AggiungiUtensile(ByVal dr As DataRow, Optional ByVal bUpdate As Boolean = False, Optional ByVal bCreaFlusso As Boolean = True)
        Dim moSql As New MecmaticaSql.Sql

        If bUpdate Then
            moSql.Where = "ARTICOLO = '" & dr("prodotti_codice") & "'"
        Else
            lIDArticolo += 1
            moSql.AddField(, "IDARTICOLO", MecmaticaSql.Sql.eFieldType.tNUMERIC, lIDArticolo, True)
        End If

        moSql.Table = "ARTICOLI"
        moSql.AddField(, "ARTICOLO", MecmaticaSql.Sql.eFieldType.tCHAR, dr("prodotti_codice"), , , False)


        Dim sDescrizioneLocal As String = "" & dr("prodotti_nomep")

        moSql.AddField(, "DESCRIZIONE", MecmaticaSql.Sql.eFieldType.tCHAR, Strings.Left(sDescrizioneLocal, 250), )
        moSql.AddField(, "IDIVA", MecmaticaSql.Sql.eFieldType.tCHAR, "022")
        moSql.AddField(, "UM", MecmaticaSql.Sql.eFieldType.tCHAR, UCase("" & dr("prodotti_UM")))

        Dim sNote As String = "" & dr("prodotti_nome")
        If sNote = "" Then
            sNote = "" & dr("prodotti_note")
        Else
            sNote += vbCrLf & "" & dr("prodotti_note")
        End If
        moSql.AddField(, "Nota", MecmaticaSql.Sql.eFieldType.tCHAR, sNote)
        moSql.AddField(, "NotaInterna", MecmaticaSql.Sql.eFieldType.tCHAR, dr("prodotti_varie"))
        moSql.AddField(, "Libero1", MecmaticaSql.Sql.eFieldType.tCHAR, dr("prodotti_id"))


        moSql.AddField(, "CATEGORIA", MecmaticaSql.Sql.eFieldType.tCHAR, dr("prodotti_codegruppimerce"))

        moSql.AddField(, "TIPO", MecmaticaSql.Sql.eFieldType.tCHAR, "A")
        moSql.AddField(, "GRUPPO", MecmaticaSql.Sql.eFieldType.tCHAR, "UTE")
        moSql.AddField(, "DEPOSITO", MecmaticaSql.Sql.eFieldType.tCHAR, "500")

        Dim lIDArticoloLocal As Long
        If Not bUpdate Then
            Dim drNew As DataRow = dsMetronomo.Tables("ARTICOLI").NewRow
            drNew("IDARTICOLO") = lIDArticolo
            drNew("ARTICOLO") = dr("prodotti_codice")
            dsMetronomo.Tables("ARTICOLI").Rows.Add(drNew)

            LogMsg("AGGIUNTO ARTICOLO " & dr("prodotti_codice"))
            moQueryPool.Add(moSql.SqlInsert)

            lIDArticoloLocal = lIDArticolo
        Else
            LogMsg("AGGIORNATO ARTICOLO " & dr("prodotti_codice"))
            moQueryPool.Add(moSql.SqlUpdate)
            lIDArticoloLocal = GetIDArticolo("" & dr("prodotti_codice"))
        End If


        '  If bCreaFlusso Then AggiungiFlusso(dr, lIDArticoloLocal)
    End Sub
    Sub AggiungiArticolo(ByVal dr As DataRow, Optional ByVal bUpdate As Boolean = False, Optional ByVal bCreaFlusso As Boolean = True)
        Dim moSql As New MecmaticaSql.Sql

        If bUpdate Then
            moSql.Where = "ARTICOLO = '" & dr("prodotti_codice") & "'"
        Else
            lIDArticolo += 1
            moSql.AddField(, "IDARTICOLO", MecmaticaSql.Sql.eFieldType.tNUMERIC, lIDArticolo, True)
        End If

        moSql.Table = "ARTICOLI"
        moSql.AddField(, "ARTICOLO", MecmaticaSql.Sql.eFieldType.tCHAR, dr("prodotti_codice"), , , False)
        'moSql.AddField(, "CodiceEsterno", MecmaticaSql.Sql.eFieldType.tCHAR, sGetCodiceEsterno("" & dr("prodotti_codiceproduttore")))

        Dim sDescrizioneLocal As String = "" & dr("prodotti_nomep")

        moSql.AddField(, "DESCRIZIONE", MecmaticaSql.Sql.eFieldType.tCHAR, Strings.Left(sDescrizioneLocal, 250), )
        moSql.AddField(, "IDIVA", MecmaticaSql.Sql.eFieldType.tCHAR, "022")
        moSql.AddField(, "UM", MecmaticaSql.Sql.eFieldType.tCHAR, UCase("" & dr("prodotti_UM")))

        Dim sNote As String = "" & dr("prodotti_nome")
        If sNote = "" Then
            sNote = "" & dr("prodotti_note")
        Else
            sNote += vbCrLf & "" & dr("prodotti_note")
        End If
        moSql.AddField(, "Nota", MecmaticaSql.Sql.eFieldType.tCHAR, sNote)
        moSql.AddField(, "NotaInterna", MecmaticaSql.Sql.eFieldType.tCHAR, dr("prodotti_varie"))
        moSql.AddField(, "Libero1", MecmaticaSql.Sql.eFieldType.tCHAR, dr("prodotti_id"))
        'moSql.AddField(, "Libero2", MecmaticaSql.Sql.eFieldType.tCHAR, sLibero2)

        moSql.AddField(, "CATEGORIA", MecmaticaSql.Sql.eFieldType.tCHAR, dr("prodotti_codegruppimerce"))

        moSql.AddField(, "TIPO", MecmaticaSql.Sql.eFieldType.tCHAR, "P")
        moSql.AddField(, "GRUPPO", MecmaticaSql.Sql.eFieldType.tCHAR, "FIN")
        moSql.AddField(, "DEPOSITO", MecmaticaSql.Sql.eFieldType.tCHAR, "200")

        Dim lIDArticoloLocal As Long
        If Not bUpdate Then
            Dim drNew As DataRow = dsMetronomo.Tables("ARTICOLI").NewRow
            drNew("IDARTICOLO") = lIDArticolo
            drNew("ARTICOLO") = dr("prodotti_codice")
            dsMetronomo.Tables("ARTICOLI").Rows.Add(drNew)

            LogMsg("AGGIUNTO ARTICOLO " & dr("prodotti_codice"))
            moQueryPool.Add(moSql.SqlInsert)

            lIDArticoloLocal = lIDArticolo
        Else
            LogMsg("AGGIORNATO ARTICOLO " & dr("prodotti_codice"))
            moQueryPool.Add(moSql.SqlUpdate)
            lIDArticoloLocal = GetIDArticolo("" & dr("prodotti_codice"))
        End If


        '  If bCreaFlusso Then AggiungiFlusso(dr, lIDArticoloLocal)
    End Sub
    Sub AggiungiFlusso(ByVal dr As DataRow, ByVal lIDArticoloMet As Long)
        Dim moSql As New MecmaticaSql.Sql
        Dim bAggiungiFaseTornitura As Boolean = False

        Dim lIDFaseLocal As Long = lGeIDFase(lIDArticoloMet, "TORNITURA")
        If lIDFaseLocal = 0 Then
            lIDFase += 1
            lIDFaseLocal = lIDFase
            bAggiungiFaseTornitura = True
        End If

        If bAggiungiFaseTornitura Then
            'AGGIUNGO FASE TORNITURA
            moSql = New MecmaticaSql.Sql
            With moSql
                .Table = "ARTICOLI_FASI"
                .AddField(, "Fase", MecmaticaSql.Sql.eFieldType.tNUMERIC, 10)
                .AddField(, "Descrizione", MecmaticaSql.Sql.eFieldType.tCHAR, "TORNITURA")
                .AddField(, "Flusso", MecmaticaSql.Sql.eFieldType.tNUMERIC, 1)
                .AddField(, "IDFase", MecmaticaSql.Sql.eFieldType.tNUMERIC, lIDFaseLocal)
                .AddField(, "OutSource", MecmaticaSql.Sql.eFieldType.tBOOLEAN, 0)
                .AddField(, "Schedulare", MecmaticaSql.Sql.eFieldType.tBOOLEAN, True)
                .AddField(, "IDArticolo", MecmaticaSql.Sql.eFieldType.tNUMERIC, lIDArticoloMet)
                '.AddField(, "TempoFase", MecmaticaSql.Sql.eFieldType.tNUMERIC, 60)
                '.AddField(, "TempoSetUp", MecmaticaSql.Sql.eFieldType.tNUMERIC, 3)
                .AddField(, "NoRisorsa", MecmaticaSql.Sql.eFieldType.tBOOLEAN, False)
                Dim sNota As String = ""
                If "" & dr("PRODOTTI_STAGIONE") <> "" Then
                    sNota = "" & dr("PRODOTTI_STAGIONE")
                End If
                If "" & dr("PRODOTTI_ANNO") <> "" Then
                    If sNota <> "" Then sNota += vbCrLf
                    sNota += "LAVORAZIONE: " & dr("PRODOTTI_ANNO")
                End If
                .AddField(, "Nota", MecmaticaSql.Sql.eFieldType.tCHAR, sNota)
            End With
            moQueryPool.Add(moSql.SqlInsert)
            LogMsg("AGGIUNTO ARTICOLO " & lIDArticoloMet & " FASE TORNITURA")

            Dim drNew As DataRow = dsMetronomo.Tables("ARTICOLI_FASI").NewRow
            drNew("IDARTICOLO") = lIDArticoloMet
            drNew("IDFase") = lIDFaseLocal
            drNew("Descrizione") = "TORNITURA"
            dsMetronomo.Tables("ARTICOLI_FASI").Rows.Add(drNew)
        End If

        'AGGIUNGO CICLO
        Dim sModello As String = "" & dr("PRODOTTIMACCHINE_CODEMACCHINA")
        sModello = sGetModello("MAC" & Format(Val(sModello), "00"))
        If sModello <> "" Then
            If lGetCiclo(lIDFaseLocal, sModello) = 0 Then
                'creo il ciclo altrimento passo 
                If lIDFaseLocal = 1075 Then Beep()
                lIDCiclo += 1
                moSql = New MecmaticaSql.Sql
                With moSql
                    .Table = "ARTICOLI_FASI_MACCHINE"
                    .AddField(, "IDFase", MecmaticaSql.Sql.eFieldType.tNUMERIC, lIDFaseLocal)
                    .AddField(, "Modello", MecmaticaSql.Sql.eFieldType.tCHAR, sModello)
                    .AddField(, "Ciclo", MecmaticaSql.Sql.eFieldType.tNUMERIC, lIDCiclo)
                    .AddField(, "TempoCiclo", MecmaticaSql.Sql.eFieldType.tNUMERIC, dr("PRODOTTIMACCHINE_SECONDIPEZZO"))
                    .AddField(, "TempoMacchina", MecmaticaSql.Sql.eFieldType.tNUMERIC, dr("PRODOTTIMACCHINE_SECONDIPEZZO"))
                    .AddField(, "OreTeoricheSetUp", MecmaticaSql.Sql.eFieldType.tNUMERIC, dr("PRODOTTIMACCHINE_OREATTREZZAGGIO"))
                    .AddField(, "Multipli", MecmaticaSql.Sql.eFieldType.tNUMERIC, 1)
                    '.AddField(, "Nota", MecmaticaSql.Sql.eFieldType.tBOOLEAN, )
                    '.AddField(, "NotaQualità", MecmaticaSql.Sql.eFieldType.tBOOLEAN, )
                End With
                moQueryPool.Add(moSql.SqlInsert)
                LogMsg("AGGIUNTO ARTICOLO " & lIDArticoloMet & " CICLO " & sModello)
                Dim drNew As DataRow = dsMetronomo.Tables("ARTICOLI_FASI_MACCHINE").NewRow
                drNew("Ciclo") = lIDCiclo
                drNew("IDFase") = lIDFaseLocal
                drNew("Modello") = sModello
                dsMetronomo.Tables("ARTICOLI_FASI_MACCHINE").Rows.Add(drNew)
            End If

        End If

        'AGGIUNGO LA FASE ESTERNA SE ESISTE
        If Val("" & dr("PRODOTTIMACCHINE_ORETRATTAMENTO")) > 0 Then
            lIDFaseLocal = lGeIDFase(lIDArticoloMet, "GENERICA EST.")
            If lIDFaseLocal = 0 Then
                lIDFase += 1
                lIDFaseLocal = lIDFase

                moSql = New MecmaticaSql.Sql
                With moSql
                    .Table = "ARTICOLI_FASI"
                    .AddField(, "Fase", MecmaticaSql.Sql.eFieldType.tNUMERIC, 20)
                    .AddField(, "Descrizione", MecmaticaSql.Sql.eFieldType.tCHAR, "GENERICA EST.")
                    .AddField(, "Flusso", MecmaticaSql.Sql.eFieldType.tNUMERIC, 1)
                    .AddField(, "IDFase", MecmaticaSql.Sql.eFieldType.tNUMERIC, lIDFaseLocal)
                    .AddField(, "OutSource", MecmaticaSql.Sql.eFieldType.tBOOLEAN, 1)
                    .AddField(, "Schedulare", MecmaticaSql.Sql.eFieldType.tBOOLEAN, False)
                    .AddField(, "IDArticolo", MecmaticaSql.Sql.eFieldType.tNUMERIC, lIDArticoloMet)
                    If Val("" & dr("PRODOTTIMACCHINE_ORETRATTAMENTO")) > 0 Then
                        .AddField(, "LeadTimeProduzione", MecmaticaSql.Sql.eFieldType.tNUMERIC, dr("PRODOTTIMACCHINE_ORETRATTAMENTO") / 60)
                    End If

                    '.AddField(, "TempoFase", MecmaticaSql.Sql.eFieldType.tNUMERIC, 3)
                    '.AddField(, "TempoSetUp", MecmaticaSql.Sql.eFieldType.tNUMERIC, 3)
                    .AddField(, "NoRisorsa", MecmaticaSql.Sql.eFieldType.tBOOLEAN, False)
                    '.AddField(, "Nota", MecmaticaSql.Sql.eFieldType.tCHAR, )
                End With
                moQueryPool.Add(moSql.SqlInsert)
                LogMsg("AGGIUNTO ARTICOLO " & lIDArticoloMet & " FASE GENERICA EST.")
                Dim drNew As DataRow = dsMetronomo.Tables("ARTICOLI_FASI").NewRow
                drNew("IDARTICOLO") = lIDArticoloMet
                drNew("IDFase") = lIDFaseLocal
                drNew("Descrizione") = "GENERICA EST."
                dsMetronomo.Tables("ARTICOLI_FASI").Rows.Add(drNew)
            End If
        End If

    End Sub
    Sub AggiungiOrdineProduzioneTestate(ByVal dr As DataRow, Optional ByVal bUpdate As Boolean = False)
        Dim moSql As New MecmaticaSql.Sql
        If dr("documenti_numero") = "4871" Then Stop
        Dim customerData As String = getCustomerData(dr("documenti_codeanagrafica"))

        With moSql

            .Table = "ORDINI_CLIENTI"
            .Where = "IDOrdineCliente=" & dr("documenti_id")
            .AddField(, "IDOrdineCliente", MecmaticaSql.Sql.eFieldType.tCHAR, dr("documenti_id"))

            .AddField(, "Data", MecmaticaSql.Sql.eFieldType.tDATETIME, dr("documenti_data"))
            .AddField(, "DataInserimento", MecmaticaSql.Sql.eFieldType.tDATETIME, dr("documenti_datacreate"))
            .AddField(, "idCliente", MecmaticaSql.Sql.eFieldType.tNUMERIC, translateCustomerCode(dr("documenti_codeanagrafica")))
            .AddField(, "ClienteRagioneSociale", MecmaticaSql.Sql.eFieldType.tCHAR, customerData.Split("#")(0))
            .AddField(, "Clienteindirizzo", MecmaticaSql.Sql.eFieldType.tCHAR, customerData.Split("#")(1))
            .AddField(, "OrdineCliente", MecmaticaSql.Sql.eFieldType.tCHAR, dr("documenti_rifordineesterno"))
            .AddField(, "Descrizione", MecmaticaSql.Sql.eFieldType.tCHAR, dr("documenti_codepagamenti"))
            .AddField(, "Attenzione", MecmaticaSql.Sql.eFieldType.tCHAR, dr("documenti_numero"))
        End With

        If bUpdate Then
            LogMsg("AGGIORNATO ORDINE " & dr("documenti_id"))
            moQueryPool.Add(moSql.SqlUpdate)
        Else
            LogMsg("AGGIUNTO ORDINE " & dr("documenti_id"))
            moQueryPool.Add(moSql.SqlInsert)
            Dim drNew As DataRow
            drNew = dsMetronomo.Tables("ORDINI_CLIENTI").NewRow
            drNew("IDOrdineCliente") = dr("documenti_id")
            '   drNew("OrdineCliente") = dr("documenti_id")
            dsMetronomo.Tables("ORDINI_CLIENTI").Rows.Add(drNew)
        End If

    End Sub
    Sub AggiungiOrdineProduzione(ByVal dr As DataRow, Optional ByVal bUpdate As Boolean = False)


        If "" & dr("documenti_numero") = "596" Then
            Beep()

        End If

        If GetIDArticolo("" & dr("documentiprodotti_codice")) = 0 Then
            'LogMsg("ARTICOLO " & dr("documentiprodotti_codice") & " NON PRESENTE IN METRONOMO! ORDINE NON IMPORTATO!")

            Dim dv As DataView = New DataView(dsImport.Tables("ARTICOLI"), "prodotti_codice = '" & dr("documentiprodotti_codice") & "'", "prodotti_codice", DataViewRowState.CurrentRows)
            If dv.Count > 0 Then
                Dim dtTemp As DataTable = dv.ToTable
                For Each dr2 As DataRow In dtTemp.Rows
                    AggiungiArticolo(dr2, False, False)
                Next
            End If

        End If

        If dr("documentiprodotti_id") = "67329" Then Beep()



        Dim moSql As New MecmaticaSql.Sql
        With moSql
            .Table = "ORDINI"
            .Where = "IDOrdine=" & dr("documentiprodotti_id")
            .AddField(, "IDOrdine", MecmaticaSql.Sql.eFieldType.tCHAR, dr("documentiprodotti_id"))
            '.AddField(, "IDProgramma", MecmaticaSql.Sql.eFieldType.tCHAR, )
            '.AddField(, "Descrizione", MecmaticaSql.Sql.eFieldType.tCHAR, )
            '.AddField(, "Contatore", MecmaticaSql.Sql.eFieldType.tNUMERIC, )
            .AddField(, "Posizione", MecmaticaSql.Sql.eFieldType.tNUMERIC, dr("documentiprodotti_order"))
            .AddField(, "IDArticolo", MecmaticaSql.Sql.eFieldType.tNUMERIC, GetIDArticolo(dr("documentiprodotti_codice")))
            '.AddField(, "CodiceEsterno", MecmaticaSql.Sql.eFieldType.tCHAR, )
            '.AddField(, "DisegnoRevisione", MecmaticaSql.Sql.eFieldType.tCHAR, )
            .AddField(, "DescrizioneArticolo", MecmaticaSql.Sql.eFieldType.tCHAR, "" & dr("documentiprodotti_nome"))
            '.AddField(, "OrdineStatus", MecmaticaSql.Sql.eFieldType.tCHAR, )
            .AddField(, "IDOrdineCliente", MecmaticaSql.Sql.eFieldType.tCHAR, dr("documentiprodotti_iddocumenti"))
            .AddField(, "OrdineCliente", MecmaticaSql.Sql.eFieldType.tCHAR, dr("documenti_numero"))
            .AddField(, "OrdineCliente2", MecmaticaSql.Sql.eFieldType.tCHAR, dr("documenti_sessionid"))
            .AddField(, "DataConsegna", MecmaticaSql.Sql.eFieldType.tDATETIME, dr("documentiprodotti_datacons"))
            Dim lIDContattoLocal As Long = GetIDContatto(dr("documenti_codeanagrafica"))
            If lIDContattoLocal > 0 Then .AddField(, "IDCliente", MecmaticaSql.Sql.eFieldType.tNUMERIC, lIDContattoLocal)
            .AddField(, "PezziTotale", MecmaticaSql.Sql.eFieldType.tNUMERIC, dr("documentiprodotti_qta"))
            .AddField(, "PezziConsegnatiExtra", MecmaticaSql.Sql.eFieldType.tNUMERIC, dr("documentiprodotti_qtaconsegnata"))
            .AddField(, "PezziResidui", MecmaticaSql.Sql.eFieldType.tNUMERIC, dr("PZResidui"))
            '.AddField(, "Nota", MecmaticaSql.Sql.eFieldType.tNUMERIC, )
            .AddField(, "Data", MecmaticaSql.Sql.eFieldType.tDATETIME, dr("documenti_data"))

        End With

        If bUpdate Then
            LogMsg("AGGIORNATO ORDINE " & dr("documenti_numero"))
            moQueryPool.Add(moSql.SqlUpdate)
        Else
            LogMsg("AGGIUNTO ORDINE " & dr("documenti_numero"))
            moQueryPool.Add(moSql.SqlInsert)
            Dim drNew As DataRow
            drNew = dsMetronomo.Tables("ORDINI").NewRow
            drNew("IDOrdine") = dr("documentiprodotti_id")
            drNew("OrdineCliente") = dr("documenti_numero")
            dsMetronomo.Tables("ORDINI").Rows.Add(drNew)
        End If

    End Sub
#End Region
#Region " Funzioni Get \ translate \ Exist"
    Function getImportCustomerExt(ByVal codiceCli As String) As DataRowView
        Using dv As New DataView(dsImport.Tables("CLIENTI_2"), "BHCCLI = '" & codiceCli & "'", "BHCCLI", DataViewRowState.CurrentRows)
            If dv.Count > 0 Then
                Return dv(0)
            Else
                Return Nothing
            End If

        End Using
    End Function
    Function getImportSupplyerExt(ByVal codiceCli As String) As DataRowView
        Using dv As New DataView(dsImport.Tables("FORNITORI_2"), "BMCFOR = '" & codiceCli & "'", "BMCFOR", DataViewRowState.CurrentRows)
            If dv.Count > 0 Then
                Return dv(0)
            Else
                Return Nothing
            End If

        End Using
    End Function
    Function getCustomerData(ByVal codiceCli As String) As String
        Using dv As New DataView(dsMetronomo.Tables("CONTATTI"), "codice = '" & codiceCli & "'", "IDContatto", DataViewRowState.CurrentRows)
            getCustomerData = dv(0)("Descrizione") & "#" & dv(0)("Indirizzo") & ", " & dv(0)("città") & ", (" & dv(0)("provincia") & ")"
        End Using
    End Function
    Function translateArtCode(ByVal codeArt As String) As Integer
        Using dv As New DataView(dsMetronomo.Tables("ARTICOLI"), "IDArticolo = '" & codeArt & "'", "IDArticolo", DataViewRowState.CurrentRows)
            translateArtCode = dv.Item(0)("IDARTICOLO")
        End Using
    End Function

    Function translateArtCodeMySQL(ByVal codeArt As String) As Integer
        Using dv As New DataView(dsMetronomo.Tables("ARTICOLI"), "ARTICOLO = '" & codeArt & "'", "IDArticolo", DataViewRowState.CurrentRows)
            translateArtCodeMySQL = dv.Item(0)("IDARTICOLO")
            If translateArtCodeMySQL = 4761 Then
                Dim a As Int16 = translateArtCodeMySQL
            End If
        End Using
    End Function
    Function translateCustomerCode(ByVal codiceCli As String) As Integer
        Using dv As New DataView(dsMetronomo.Tables("CONTATTI"), "codice = '" & codiceCli & "'", "IDContatto", DataViewRowState.CurrentRows)
            translateCustomerCode = dv.Item(0)(0)
        End Using
    End Function
    Function bArticoloExists(ByVal sArticolo As String, Optional ByRef lIDArticoloLocal As Long = 0, Optional ByRef bUpdate As Boolean = False) As Boolean
        If sArticolo = "" Then
            Return True
        End If
        Dim sArticoloTemp As String = ""
        Dim dv As New DataView(dsMetronomo.Tables("ARTICOLI"), "IDARTICOLO = " & lIDArticoloLocal, "IDArticolo", DataViewRowState.CurrentRows)
        If dv.Count > 0 Then
            sArticoloTemp = dv(0)("Articolo")
        End If
        dv.Dispose()
        If sArticoloTemp <> "" Then
            If RTrim(UCase(sArticolo)) <> RTrim(UCase(sArticoloTemp)) Then
                bUpdate = True
            End If
        End If
        dv = New DataView(dsMetronomo.Tables("ARTICOLI"), "ARTICOLO = '" & mecDatabase.sString2SQL(sArticolo) & "'", "Articolo", DataViewRowState.CurrentRows)
        If dv.Count > 0 Then
            bUpdate = True
            Return True
        Else
            Return False
        End If
    End Function
    Function bOrdineTestateExist(ByVal sCodice As String) As Boolean
        If sCodice = "" Then
            Return True
        End If
        Using dv As New DataView(dsMetronomo.Tables("ORDINI_CLIENTI"), "IDORDINECLIENTE = '" & sCodice & "'", "IDOrdineCliente", DataViewRowState.CurrentRows)
            bOrdineTestateExist = dv.Count > 0
        End Using
    End Function
    Function bOrdineExist(ByVal sCodice As String) As Boolean
        If sCodice = "" Then
            Return True
        End If
        Using dv As New DataView(dsMetronomo.Tables("ORDINI"), "IDORDINE = '" & sCodice & "'", "IDOrdine", DataViewRowState.CurrentRows)
            bOrdineExist = dv.Count > 0
        End Using
    End Function

    Function bOrdineImportExist(ByVal sCodice As String) As Boolean
        If sCodice = "" Then
            Return True
        End If
        Using dv As New DataView(dsImport.Tables("ORDINI"), "documentiprodotti_id = '" & sCodice & "'", "documentiprodotti_id", DataViewRowState.CurrentRows)
            bOrdineImportExist = dv.Count > 0
        End Using
    End Function
    Function lGetCiclo(ByVal lIDFaseLocal As Long, sModello As String) As Long
        Using dv As DataView = New DataView(dsMetronomo.Tables("ARTICOLI_FASI_MACCHINE"), "IDFase = " & lIDFaseLocal & " AND Modello='" & sModello & "'", "Ciclo", DataViewRowState.CurrentRows)
            If dv.Count > 0 Then
                Return dv(0)("Ciclo")
            Else
                Return 0
            End If
        End Using
    End Function
    Function lGeIDFase(ByVal lIDArticoloLocal As Long, sFaseDesc As String) As Long
        Using dv As DataView = New DataView(dsMetronomo.Tables("ARTICOLI_FASI"), "IDARTICOLO = " & lIDArticoloLocal & " AND Descrizione='" & sFaseDesc & "'", "IDFase", DataViewRowState.CurrentRows)
            If dv.Count > 0 Then
                Return dv(0)("IDFase")
            Else
                Return 0
            End If
        End Using
    End Function
    Function sGetModello(ByVal sCodice As String) As String
        Using dv As DataView = New DataView(dsMetronomo.Tables("MACCHINE"), "MACCHINA = '" & mecDatabase.sString2SQL(sCodice) & "'", "Macchina", DataViewRowState.CurrentRows)
            If dv.Count > 0 Then
                Return "" & dv(0)("Modello")
            Else
                Return ""
            End If
        End Using
    End Function
    Function GetIDContatto(ByVal sCodice As String) As Integer
        Using dv As DataView = New DataView(dsMetronomo.Tables("CONTATTI"), "Codice = '" & mecDatabase.sString2SQL(sCodice) & "'", "Codice", DataViewRowState.CurrentRows)
            If dv.Count > 0 Then
                Return dv(0)("IDContatto")
            Else
                Return 0
            End If
        End Using
    End Function
    Function GetIDArticolo(ByVal sCodice As String) As Integer
        Using dv As DataView = New DataView(dsMetronomo.Tables("ARTICOLI"), "Articolo = '" & mecDatabase.sString2SQL(sCodice) & "'", "Articolo", DataViewRowState.CurrentRows)
            If dv.Count > 0 Then
                Return dv(0)("IDArticolo")
            Else
                Return 0
            End If
        End Using
    End Function






#End Region
#End Region
#Region "Sub NUKE"
    Sub Nuke()
        moQueryPool.Clear()
        moQueryPool.Add("DELETE FROM ARTICOLI_FASI")
        'moQueryPool.Add("DELETE FROM ARTICOLI_COMPOSIZIONE")
        'moQueryPool.Add("DELETE FROM MACCHINE_MODELLI")
        'moQueryPool.Add("DELETE FROM DDT_DETTAGLIO")
        'moQueryPool.Add("DELETE FROM DDT")
        'moQueryPool.Add("DELETE FROM DDT_CARICO_DETTAGLIO")
        'moQueryPool.Add("DELETE FROM DDT_CARICO")
        moQueryPool.Add("DELETE FROM ORDINI")
        'moQueryPool.Add("DELETE FROM ORDINI_CLIENTI")
        'moQueryPool.Add("DELETE FROM OFFERTE_DETTAGLIO")
        'moQueryPool.Add("DELETE FROM OFFERTE")
        'moQueryPool.Add("DELETE FROM PREVENTIVI_DETTAGLIO")
        'moQueryPool.Add("DELETE FROM FASI WHERE DESCFASE <> 'LAVORAZIONE' AND DESCFASE <> 'LAVORAZIONE ESTERNA'")
        'moQueryPool.Add("DELETE FROM PREVENTIVI_LISTINI_VENDITA_DETTAGLIO")
        'moQueryPool.Add("DELETE FROM PREVENTIVI")
        'moQueryPool.Add("DELETE FROM LISTINI_DETTAGLIO")
        'moQueryPool.Add("UPDATE CONTATTI SET IDListinoVendita=NULL")
        'moQueryPool.Add("DELETE FROM LISTINI WHERE IDLISTINO<>'4'")
        'moQueryPool.Add("DELETE CONTATTI_DOCUMENTI_NOTE")
        'moQueryPool.Add("DELETE FROM ODL")
        'moQueryPool.Add("DELETE FROM COMMESSE_FABBISOGNO")
        'moQueryPool.Add("DELETE FROM COMMESSE_IMPEGNI")
        'moQueryPool.Add("DELETE FROM COMMESSE")
        'moQueryPool.Add("DELETE FROM ARTICOLI_FASI_MACCHINE")
        'moQueryPool.Add("DELETE FROM ARTICOLI_FASI_FORNITORI")
        'moQueryPool.Add("DELETE FROM ARTICOLI_FASI")
        moQueryPool.Add("DELETE FROM ARTICOLI where articolo<>'TEST'")
        'moQueryPool.Add("DELETE FROM MATERIALE")
        'moQueryPool.Add("DELETE FROM RUBRICA")
        moQueryPool.Add("DELETE FROM CONTATTI")
        'moQueryPool.Add("DELETE BANCHE WHERE IDBanca<>'CASSA' and IDBanca<>'B9' and IDBanca<>'B10'")

        If EseguiInserimento(moQueryPool) Then
            moQueryPool.Clear()
            moQueryPool = New ArrayList
        Else
            moQueryPool.Clear()
            Exit Sub
        End If
    End Sub
#End Region

    Sub CaricaExcel(ByVal sFile As String, ByVal sAlias As String)
        'If sFile = "" Then
        '    Dim ofd As New OpenFileDialog
        '    ofd.ShowDialog()
        '    sFile = ofd.FileName
        'End If
        If sFile = "" Then
            Exit Sub
        End If
        Dim smoTemp(1) As String
        Dim moItem As New ListViewItem
        Dim dt As DataTable
        Dim da As OleDb.OleDbDataAdapter
        Dim cn As New OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & sFile & ";Extended Properties=""Excel 12.0 Xml;HDR=YES"";")
        Try
            cn.Open()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Dim sTableName As String

        Dim dtSchema As DataTable = cn.GetOleDbSchemaTable(OleDb.OleDbSchemaGuid.Tables, New Object() {Nothing, Nothing, Nothing, "TABLE"})
        For Each dr In dtSchema.Rows
            Try
                ' If dr("TABLE_NAME").Contains("heet1") Then
                sTableName = GetTableName(dr("TABLE_NAME"))
                da = New OleDb.OleDbDataAdapter("SELECT * FROM [" & dr("TABLE_NAME") & "]", cn)
                dt = New DataTable(sAlias)
                da.Fill(dt)
                dsImport.Tables.Add(dt)
                smoTemp(1) = "Import"
                smoTemp(0) = sAlias
                moItem = New ListViewItem(smoTemp)
                AddTable(moItem)
                '  End If
                'dbgFields.DataSource = dt
            Catch ex As Exception
                MsgBox(ex.Message)

            End Try
        Next
        'Dim da As New OleDb.OleDbDataAdapter("", cn)

    End Sub

    Private Sub Label6_Click(sender As Object, e As EventArgs) Handles Label6.Click
        Dim openFileDialog1 As New FolderBrowserDialog()
        ' openFileDialog1.RootFolder = Environment.SpecialFolder.Personal


        If openFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            SurceFilesPath.Text = openFileDialog1.SelectedPath
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click


        Try
            Carica(True)
            LogMsg("INIZIO ESPORTAZIONE DDT!!!")
            Dim dtt As DataTable = dsMetronomo.Tables("DDT").Copy()
            For Each dr As DataRow In dtt.Rows
                If dr("9") = "V" Then
                    Dim descr = dr("31").ToString.WordWrap(35)
                    dr("23") = descr(0)
                Else
                    Dim descr = dr("31").ToString.WordWrap(35)
                    For i = 1 To descr.Count - 1
                        If i <= 7 Then
                            dr(CStr(23 + i)) = descr(i)
                        End If
                    Next

                End If
            Next
            dtt.Columns().Remove("31")
            dtt.NPOIToExcel("\\serverhp\Contab\MAP\Trasferimenti\BOLLE\DDT_ESPORTATI_" & Month(Today) & "_" & Year(Today) & ".xlsx")
            LogMsg("FINE ESPORTAZIONE DDT!!!")
            sbloccaDDT()
            Carica(True)
            LogMsg("DDT BLOCCATI!!!")
        Catch ex As Exception
            LogMsg(ex.Message)

        End Try


        's.WordCount()
    End Sub

    Private Sub sbloccaDDT()
        Dim ddtK As String
        For Each dr As DataRow In dsMetronomo.Tables("DDT").Rows
            If ddtK Is Nothing Or ddtK <> dr("11") Then
                OpenRowDDT(dr)
                ddtK = dr("11")
            End If

        Next
        Check_Query(True)
    End Sub
    Sub OpenRowDDT(ByVal dr As DataRow)
        Dim moSql As New MecmaticaSql.Sql
        moSql.Where = "IDDDT = '" & dr("11") & "'"
        moSql.Table = "DDT"
        moSql.AddField(, "REGISTRATA", MecmaticaSql.Sql.eFieldType.tCHAR, "ADMINISTRATOR " & Now.ToLongDateString)
        moSql.AddField(, "STATUSINTEROP", MecmaticaSql.Sql.eFieldType.tBOOLEAN, True)
        moQueryPool.Add(moSql.SqlUpdate)

    End Sub

    Private Sub Button1_Disposed(sender As Object, e As EventArgs) Handles Button1.Disposed

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Carica(True)

        da = New SqlClient.SqlDataAdapter()
        da.SelectCommand = New SqlClient.SqlCommand("select idddt, data, DestinatarioRagioneSociale, StatusInterop from DDT where StatusInterop=1 and causale='VENDITA'")
        da.SelectCommand.Connection = cnMetronomo
        Dim cb As New SqlClient.SqlCommandBuilder(da)
        da.UpdateCommand = cb.GetUpdateCommand(True)
        da.DeleteCommand = cb.GetDeleteCommand(True)
        da.InsertCommand = cb.GetInsertCommand(True)

        Dim dt As New DataTable("DDT_EXP")
        da.Fill(dt)
        dsddt = New DataSet()
        dsddt.Tables.Add(dt)
        DDTEXP.DataSource = dt
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        da.Update(dsddt.Tables(0))

    End Sub
End Class
