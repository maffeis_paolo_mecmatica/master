﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.tabMenu = New System.Windows.Forms.TabControl()
        Me.pagPrincipale = New System.Windows.Forms.TabPage()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.pgbProgresso = New System.Windows.Forms.ProgressBar()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.clbFunzioni = New System.Windows.Forms.CheckedListBox()
        Me.btnEsci = New System.Windows.Forms.Button()
        Me.btnEsegui = New System.Windows.Forms.Button()
        Me.txtLogMain = New System.Windows.Forms.TextBox()
        Me.txtLog = New System.Windows.Forms.TextBox()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.btnCarica = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.dbgFields = New System.Windows.Forms.DataGridView()
        Me.lstTabelle = New System.Windows.Forms.ListView()
        Me.Tabella = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.DB = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.pagConfig = New System.Windows.Forms.TabPage()
        Me.SurceFilesPath = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtImportPassword = New System.Windows.Forms.TextBox()
        Me.txtImportUser = New System.Windows.Forms.TextBox()
        Me.txtImportServer = New System.Windows.Forms.TextBox()
        Me.txtImportDB = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtTimerImportazione = New System.Windows.Forms.TextBox()
        Me.btnSalva = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtMetronomoPassword = New System.Windows.Forms.TextBox()
        Me.txtMetronomoUser = New System.Windows.Forms.TextBox()
        Me.txtMetronomoServer = New System.Windows.Forms.TextBox()
        Me.txtMetronomoDB = New System.Windows.Forms.TextBox()
        Me.Esportati = New System.Windows.Forms.TabPage()
        Me.DDTEXP = New System.Windows.Forms.DataGridView()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.tabMenu.SuspendLayout()
        Me.pagPrincipale.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.dbgFields, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pagConfig.SuspendLayout()
        Me.Esportati.SuspendLayout()
        CType(Me.DDTEXP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tabMenu
        '
        Me.tabMenu.Controls.Add(Me.pagPrincipale)
        Me.tabMenu.Controls.Add(Me.Esportati)
        Me.tabMenu.Controls.Add(Me.TabPage1)
        Me.tabMenu.Controls.Add(Me.pagConfig)
        Me.tabMenu.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabMenu.Location = New System.Drawing.Point(0, 0)
        Me.tabMenu.Name = "tabMenu"
        Me.tabMenu.SelectedIndex = 0
        Me.tabMenu.Size = New System.Drawing.Size(934, 562)
        Me.tabMenu.TabIndex = 0
        '
        'pagPrincipale
        '
        Me.pagPrincipale.BackColor = System.Drawing.Color.Transparent
        Me.pagPrincipale.Controls.Add(Me.Button1)
        Me.pagPrincipale.Controls.Add(Me.pgbProgresso)
        Me.pagPrincipale.Controls.Add(Me.Label2)
        Me.pagPrincipale.Controls.Add(Me.Label1)
        Me.pagPrincipale.Controls.Add(Me.clbFunzioni)
        Me.pagPrincipale.Controls.Add(Me.btnEsci)
        Me.pagPrincipale.Controls.Add(Me.btnEsegui)
        Me.pagPrincipale.Controls.Add(Me.txtLogMain)
        Me.pagPrincipale.Controls.Add(Me.txtLog)
        Me.pagPrincipale.Location = New System.Drawing.Point(4, 22)
        Me.pagPrincipale.Name = "pagPrincipale"
        Me.pagPrincipale.Padding = New System.Windows.Forms.Padding(3)
        Me.pagPrincipale.Size = New System.Drawing.Size(926, 536)
        Me.pagPrincipale.TabIndex = 0
        Me.pagPrincipale.Text = "Principale"
        Me.pagPrincipale.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button1.Location = New System.Drawing.Point(8, 477)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(226, 23)
        Me.Button1.TabIndex = 7
        Me.Button1.Text = "Esporta DDT"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'pgbProgresso
        '
        Me.pgbProgresso.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pgbProgresso.Location = New System.Drawing.Point(240, 505)
        Me.pgbProgresso.Name = "pgbProgresso"
        Me.pgbProgresso.Size = New System.Drawing.Size(678, 23)
        Me.pgbProgresso.TabIndex = 6
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(240, 6)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(28, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Log:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Funzioni:"
        '
        'clbFunzioni
        '
        Me.clbFunzioni.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.clbFunzioni.FormattingEnabled = True
        Me.clbFunzioni.Location = New System.Drawing.Point(8, 24)
        Me.clbFunzioni.Name = "clbFunzioni"
        Me.clbFunzioni.Size = New System.Drawing.Size(226, 409)
        Me.clbFunzioni.TabIndex = 4
        '
        'btnEsci
        '
        Me.btnEsci.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEsci.Location = New System.Drawing.Point(8, 505)
        Me.btnEsci.Name = "btnEsci"
        Me.btnEsci.Size = New System.Drawing.Size(226, 23)
        Me.btnEsci.TabIndex = 3
        Me.btnEsci.Text = "Esci"
        Me.btnEsci.UseVisualStyleBackColor = True
        '
        'btnEsegui
        '
        Me.btnEsegui.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEsegui.Location = New System.Drawing.Point(8, 448)
        Me.btnEsegui.Name = "btnEsegui"
        Me.btnEsegui.Size = New System.Drawing.Size(226, 23)
        Me.btnEsegui.TabIndex = 3
        Me.btnEsegui.Text = "Esegui"
        Me.btnEsegui.UseVisualStyleBackColor = True
        '
        'txtLogMain
        '
        Me.txtLogMain.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtLogMain.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLogMain.Location = New System.Drawing.Point(655, 22)
        Me.txtLogMain.Multiline = True
        Me.txtLogMain.Name = "txtLogMain"
        Me.txtLogMain.Size = New System.Drawing.Size(263, 477)
        Me.txtLogMain.TabIndex = 2
        '
        'txtLog
        '
        Me.txtLog.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtLog.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLog.Location = New System.Drawing.Point(240, 22)
        Me.txtLog.Multiline = True
        Me.txtLog.Name = "txtLog"
        Me.txtLog.Size = New System.Drawing.Size(409, 477)
        Me.txtLog.TabIndex = 2
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.btnCarica)
        Me.TabPage1.Controls.Add(Me.Label9)
        Me.TabPage1.Controls.Add(Me.Label11)
        Me.TabPage1.Controls.Add(Me.dbgFields)
        Me.TabPage1.Controls.Add(Me.lstTabelle)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(926, 536)
        Me.TabPage1.TabIndex = 2
        Me.TabPage1.Text = "Tabelle"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'btnCarica
        '
        Me.btnCarica.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCarica.Location = New System.Drawing.Point(9, 483)
        Me.btnCarica.Name = "btnCarica"
        Me.btnCarica.Size = New System.Drawing.Size(226, 23)
        Me.btnCarica.TabIndex = 8
        Me.btnCarica.Text = "Carica"
        Me.btnCarica.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(237, 14)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(93, 13)
        Me.Label9.TabIndex = 7
        Me.Label9.Text = "Contenuto tabella:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(6, 14)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(45, 13)
        Me.Label11.TabIndex = 6
        Me.Label11.Text = "Tabelle:"
        '
        'dbgFields
        '
        Me.dbgFields.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dbgFields.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dbgFields.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dbgFields.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dbgFields.Location = New System.Drawing.Point(240, 30)
        Me.dbgFields.Name = "dbgFields"
        Me.dbgFields.Size = New System.Drawing.Size(678, 476)
        Me.dbgFields.TabIndex = 3
        '
        'lstTabelle
        '
        Me.lstTabelle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lstTabelle.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.Tabella, Me.DB})
        Me.lstTabelle.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstTabelle.HideSelection = False
        Me.lstTabelle.Location = New System.Drawing.Point(8, 30)
        Me.lstTabelle.Name = "lstTabelle"
        Me.lstTabelle.Size = New System.Drawing.Size(226, 445)
        Me.lstTabelle.TabIndex = 2
        Me.lstTabelle.UseCompatibleStateImageBehavior = False
        Me.lstTabelle.View = System.Windows.Forms.View.Details
        '
        'Tabella
        '
        Me.Tabella.Text = "Tabella"
        Me.Tabella.Width = 140
        '
        'DB
        '
        Me.DB.Text = "DB"
        '
        'pagConfig
        '
        Me.pagConfig.BackColor = System.Drawing.Color.Transparent
        Me.pagConfig.Controls.Add(Me.SurceFilesPath)
        Me.pagConfig.Controls.Add(Me.Label6)
        Me.pagConfig.Controls.Add(Me.Label3)
        Me.pagConfig.Controls.Add(Me.Label12)
        Me.pagConfig.Controls.Add(Me.Label13)
        Me.pagConfig.Controls.Add(Me.Label14)
        Me.pagConfig.Controls.Add(Me.txtImportPassword)
        Me.pagConfig.Controls.Add(Me.txtImportUser)
        Me.pagConfig.Controls.Add(Me.txtImportServer)
        Me.pagConfig.Controls.Add(Me.txtImportDB)
        Me.pagConfig.Controls.Add(Me.Label7)
        Me.pagConfig.Controls.Add(Me.txtTimerImportazione)
        Me.pagConfig.Controls.Add(Me.btnSalva)
        Me.pagConfig.Controls.Add(Me.Label10)
        Me.pagConfig.Controls.Add(Me.Label8)
        Me.pagConfig.Controls.Add(Me.Label5)
        Me.pagConfig.Controls.Add(Me.Label4)
        Me.pagConfig.Controls.Add(Me.txtMetronomoPassword)
        Me.pagConfig.Controls.Add(Me.txtMetronomoUser)
        Me.pagConfig.Controls.Add(Me.txtMetronomoServer)
        Me.pagConfig.Controls.Add(Me.txtMetronomoDB)
        Me.pagConfig.Location = New System.Drawing.Point(4, 22)
        Me.pagConfig.Name = "pagConfig"
        Me.pagConfig.Padding = New System.Windows.Forms.Padding(3)
        Me.pagConfig.Size = New System.Drawing.Size(926, 536)
        Me.pagConfig.TabIndex = 1
        Me.pagConfig.Text = "Configurazione"
        Me.pagConfig.UseVisualStyleBackColor = True
        '
        'SurceFilesPath
        '
        Me.SurceFilesPath.Location = New System.Drawing.Point(167, 122)
        Me.SurceFilesPath.Name = "SurceFilesPath"
        Me.SurceFilesPath.Size = New System.Drawing.Size(544, 20)
        Me.SurceFilesPath.TabIndex = 34
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(9, 126)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(94, 13)
        Me.Label6.TabIndex = 33
        Me.Label6.Text = "Cartella File Fonte:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(390, 91)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(106, 13)
        Me.Label3.TabIndex = 30
        Me.Label3.Text = "Password DB Import:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(390, 65)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(82, 13)
        Me.Label12.TabIndex = 31
        Me.Label12.Text = "User DB Import:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(390, 39)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(88, 13)
        Me.Label13.TabIndex = 32
        Me.Label13.Text = "Database Import:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(390, 13)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(73, 13)
        Me.Label14.TabIndex = 29
        Me.Label14.Text = "Server Import:"
        '
        'txtImportPassword
        '
        Me.txtImportPassword.Location = New System.Drawing.Point(549, 88)
        Me.txtImportPassword.Name = "txtImportPassword"
        Me.txtImportPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtImportPassword.Size = New System.Drawing.Size(162, 20)
        Me.txtImportPassword.TabIndex = 25
        '
        'txtImportUser
        '
        Me.txtImportUser.Location = New System.Drawing.Point(549, 62)
        Me.txtImportUser.Name = "txtImportUser"
        Me.txtImportUser.Size = New System.Drawing.Size(162, 20)
        Me.txtImportUser.TabIndex = 26
        '
        'txtImportServer
        '
        Me.txtImportServer.Location = New System.Drawing.Point(549, 10)
        Me.txtImportServer.Name = "txtImportServer"
        Me.txtImportServer.Size = New System.Drawing.Size(162, 20)
        Me.txtImportServer.TabIndex = 27
        '
        'txtImportDB
        '
        Me.txtImportDB.Location = New System.Drawing.Point(549, 36)
        Me.txtImportDB.Name = "txtImportDB"
        Me.txtImportDB.Size = New System.Drawing.Size(162, 20)
        Me.txtImportDB.TabIndex = 28
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(9, 166)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(123, 13)
        Me.Label7.TabIndex = 24
        Me.Label7.Text = "Timer importazione (min):"
        '
        'txtTimerImportazione
        '
        Me.txtTimerImportazione.Location = New System.Drawing.Point(167, 163)
        Me.txtTimerImportazione.Name = "txtTimerImportazione"
        Me.txtTimerImportazione.Size = New System.Drawing.Size(58, 20)
        Me.txtTimerImportazione.TabIndex = 23
        '
        'btnSalva
        '
        Me.btnSalva.Location = New System.Drawing.Point(8, 207)
        Me.btnSalva.Name = "btnSalva"
        Me.btnSalva.Size = New System.Drawing.Size(156, 23)
        Me.btnSalva.TabIndex = 8
        Me.btnSalva.Text = "Salva configurazione"
        Me.btnSalva.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(8, 91)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(139, 13)
        Me.Label10.TabIndex = 7
        Me.Label10.Text = "Password  SQL Metronomo:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(8, 65)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(112, 13)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "User SQL Metronomo:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(8, 39)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(112, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Database Metronomo:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 13)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(97, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Server Metronomo:"
        '
        'txtMetronomoPassword
        '
        Me.txtMetronomoPassword.Location = New System.Drawing.Point(167, 88)
        Me.txtMetronomoPassword.Name = "txtMetronomoPassword"
        Me.txtMetronomoPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtMetronomoPassword.Size = New System.Drawing.Size(162, 20)
        Me.txtMetronomoPassword.TabIndex = 0
        '
        'txtMetronomoUser
        '
        Me.txtMetronomoUser.Location = New System.Drawing.Point(167, 62)
        Me.txtMetronomoUser.Name = "txtMetronomoUser"
        Me.txtMetronomoUser.Size = New System.Drawing.Size(162, 20)
        Me.txtMetronomoUser.TabIndex = 0
        '
        'txtMetronomoServer
        '
        Me.txtMetronomoServer.Location = New System.Drawing.Point(167, 10)
        Me.txtMetronomoServer.Name = "txtMetronomoServer"
        Me.txtMetronomoServer.Size = New System.Drawing.Size(162, 20)
        Me.txtMetronomoServer.TabIndex = 0
        '
        'txtMetronomoDB
        '
        Me.txtMetronomoDB.Location = New System.Drawing.Point(167, 36)
        Me.txtMetronomoDB.Name = "txtMetronomoDB"
        Me.txtMetronomoDB.Size = New System.Drawing.Size(162, 20)
        Me.txtMetronomoDB.TabIndex = 0
        '
        'Esportati
        '
        Me.Esportati.Controls.Add(Me.Button3)
        Me.Esportati.Controls.Add(Me.Button2)
        Me.Esportati.Controls.Add(Me.DDTEXP)
        Me.Esportati.Location = New System.Drawing.Point(4, 22)
        Me.Esportati.Name = "Esportati"
        Me.Esportati.Size = New System.Drawing.Size(926, 536)
        Me.Esportati.TabIndex = 3
        Me.Esportati.Text = "DDT Esportati"
        Me.Esportati.UseVisualStyleBackColor = True
        '
        'DDTEXP
        '
        Me.DDTEXP.AllowUserToAddRows = False
        Me.DDTEXP.AllowUserToDeleteRows = False
        Me.DDTEXP.AllowUserToOrderColumns = True
        Me.DDTEXP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DDTEXP.Location = New System.Drawing.Point(8, 3)
        Me.DDTEXP.Name = "DDTEXP"
        Me.DDTEXP.Size = New System.Drawing.Size(910, 439)
        Me.DDTEXP.TabIndex = 0
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(8, 448)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(910, 36)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "CARICA"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(8, 490)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(910, 37)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "SALVA"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(934, 562)
        Me.Controls.Add(Me.tabMenu)
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ImportExport Deco"
        Me.tabMenu.ResumeLayout(False)
        Me.pagPrincipale.ResumeLayout(False)
        Me.pagPrincipale.PerformLayout()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.dbgFields, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pagConfig.ResumeLayout(False)
        Me.pagConfig.PerformLayout()
        Me.Esportati.ResumeLayout(False)
        CType(Me.DDTEXP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tabMenu As System.Windows.Forms.TabControl
    Friend WithEvents pagPrincipale As System.Windows.Forms.TabPage
    Friend WithEvents txtLogMain As System.Windows.Forms.TextBox
    Friend WithEvents txtLog As System.Windows.Forms.TextBox
    Friend WithEvents clbFunzioni As System.Windows.Forms.CheckedListBox
    Friend WithEvents btnEsegui As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnEsci As System.Windows.Forms.Button
    Friend WithEvents pgbProgresso As System.Windows.Forms.ProgressBar
    Friend WithEvents txtDNS2 As System.Windows.Forms.TextBox
    Friend WithEvents pagConfig As System.Windows.Forms.TabPage
    Friend WithEvents btnSalva As System.Windows.Forms.Button
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtMetronomoPassword As System.Windows.Forms.TextBox
    Friend WithEvents txtMetronomoUser As System.Windows.Forms.TextBox
    Friend WithEvents txtMetronomoServer As System.Windows.Forms.TextBox
    Friend WithEvents txtMetronomoDB As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtTimerImportazione As System.Windows.Forms.TextBox
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents dbgFields As System.Windows.Forms.DataGridView
    Friend WithEvents lstTabelle As System.Windows.Forms.ListView
    Friend WithEvents Tabella As System.Windows.Forms.ColumnHeader
    Friend WithEvents DB As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnCarica As System.Windows.Forms.Button
    Friend WithEvents Label3 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents txtImportPassword As TextBox
    Friend WithEvents txtImportUser As TextBox
    Friend WithEvents txtImportServer As TextBox
    Friend WithEvents txtImportDB As TextBox
    Friend WithEvents SurceFilesPath As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents Esportati As TabPage
    Friend WithEvents DDTEXP As DataGridView
    Friend WithEvents Button2 As Button
    Friend WithEvents Button3 As Button
End Class
